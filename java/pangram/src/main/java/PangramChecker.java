public class PangramChecker {

  public boolean isPangram(final String input) {
    return input.toLowerCase()
        .replaceAll("[^a-z]", "")
        .chars()
        .distinct()
        .count() == 26;
  }
}
