class SqueakyClean {
    static String clean(String identifier) {
        if (identifier.isEmpty()) { return ""; }
        else {
            char[] chars = identifier.toCharArray();
            StringBuilder builder = new StringBuilder();
            boolean caseId = false;
            for ( int i = 0; i < identifier.length(); i++ ) {
                switch (chars[i]) {
                    // Replace any spaces encountered with underscores
                    case ' ' -> builder.append('_');
                    case '_' -> builder.append('_');

                    // leetspeak to normal text
                    case '4' -> builder.append('a');
                    case '3' -> builder.append('e');
                    case '0' -> builder.append('o');
                    case '1' -> builder.append('l');
                    case '7' -> builder.append('t');

                    // kebab-case to camelCase
                    case '-' -> caseId = true;

                    default -> {
                        if (Character.isAlphabetic(chars[i])) {
                            if (caseId) {
                                builder.append(Character.toUpperCase(chars[i]));
                                caseId = false;
                            } else {
                                builder.append(chars[i]);
                            }

                        }
                    }
                }
            }
            return builder.toString();
        }
    }
}
