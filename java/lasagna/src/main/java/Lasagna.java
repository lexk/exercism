public class Lasagna {
    public int expectedMinutesInOven() { return 40; }

    public int remainingMinutesInOven(int min_passed) { return expectedMinutesInOven() - min_passed; }

    public int preparationTimeInMinutes(int layers) { return layers *2; }

    public int totalTimeInMinutes(int layers, int time_passed) { return preparationTimeInMinutes(layers) + time_passed; }
}