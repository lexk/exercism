section .text
global leap_year
leap_year:
    mov rcx,4
    mov rdx,0
    mov rax,leap_year 
    div rcx
    cmp rdx,0
    je hundred
    ret 0
hundred:
    mov rcx,100
    mov rdx,0
    mov rax,leap_year
    div rcx
    cmp rdx,0
    je fourhundred    
    ret 1
fourhundred:
    mov rcx,400
    mov rdx,0
    mov rax,leap_year
    div rcx
    cmp rdx,0
    je leap_400
    ret 0

leap_400:
    ret 1
    
    
