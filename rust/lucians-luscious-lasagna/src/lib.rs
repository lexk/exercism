// This stub file contains items which aren't used yet; feel free to remove this module attribute
// to enable stricter warnings.
#![allow(unused)]

const EX_MIN_OVEN_T: i32 = 40;
const MIN_PER_LAYER: i32 = 2;


pub fn expected_minutes_in_oven() -> i32 {
    EX_MIN_OVEN_T
}

pub fn remaining_minutes_in_oven(actual_minutes_in_oven: i32) -> i32 {
    let mut remain_in_oven = expected_minutes_in_oven() - actual_minutes_in_oven;
    remain_in_oven
}

pub fn preparation_time_in_minutes(number_of_layers: i32) -> i32 {
    number_of_layers * MIN_PER_LAYER
}

pub fn elapsed_time_in_minutes(number_of_layers: i32, actual_minutes_in_oven: i32) -> i32 {
    let elapsed_t = preparation_time_in_minutes(number_of_layers) + actual_minutes_in_oven;
    elapsed_t
}
