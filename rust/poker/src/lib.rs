use std::cmp::Ordering;
use std::collections::{HashMap};
use crate::HandRank::{Flush, FourOfAKind, FullHouse, HighCard, OnePair, Straight, StraightFlush,
                      ThreeOfAKind, TwoPair};
use crate::Rank::{ACE, EIGHT, FIVE, FOUR, JACK, KING, NINE, QUEEN, SEVEN, SIX, TEN, THREE, TWO};
use crate::Suit::{CLUB, DIAMOND, HEART, SPADE};

/// Given a list of poker hands, return a list of those hands which win.
///
/// Note the type signature: this function should return _the same_ reference to
/// the winning hand(s) as were passed in, not reconstructed strings which happen to be equal.

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Copy, Clone)]
pub enum Suit {
    CLUB,
    SPADE,
    HEART,
    DIAMOND
}

impl Suit {
    pub fn from_str(s: &str) -> Result<Suit, ()> {
        match s {
            "C" => Ok(CLUB),
            "S" => Ok(SPADE),
            "H" => Ok(HEART),
            "D" => Ok(DIAMOND),
            _ => Err(()),
        }
    }
}


#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Copy, Clone)]
pub enum Rank {
    TWO = 2, THREE = 3, FOUR = 4, FIVE = 5, SIX = 6, SEVEN = 7, EIGHT = 8, NINE = 9, TEN = 10,
    JACK = 11, QUEEN = 12, KING = 13, ACE = 14,
}

impl Rank {
    pub fn from_str(s: &str) -> Result<Rank, ()> {
        match s {
            "2" => Ok(TWO),
            "3" => Ok(THREE),
            "4" => Ok(FOUR),
            "5" => Ok(FIVE),
            "6" => Ok(SIX),
            "7" => Ok(SEVEN),
            "8" => Ok(EIGHT),
            "9" => Ok(NINE),
            "10" => Ok(TEN),
            "J" => Ok(JACK),
            "Q" => Ok(QUEEN),
            "K" => Ok(KING),
            "A" => Ok(ACE),
            _ => Err(())
        }
    }
    pub fn into_i32(self) -> i32 {
        self as i32
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Copy, Clone)]
pub struct Card {
    rank: Rank,
    suit: Suit
}

impl Card {
    pub fn from_tuple_rank_suit(tuple_card: (Rank, Suit)) -> Card {
        Card{rank: tuple_card.0, suit: tuple_card.1}
    }
    pub fn from_tuple_two_str(tuple_card: (&str, &str)) -> Card {
        Card {
            rank: Rank::from_str(tuple_card.0).unwrap(),
            suit: Suit::from_str(tuple_card.1).unwrap()
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord)]
pub struct Hand {
    hand: Vec<Card>,
}

impl Hand {
    pub fn sort(mut self) -> Hand {
        self.hand.sort();
        self
    }
    pub fn sum(&self) -> i32 {
        self.hand.iter().map(|&card| card.rank.clone() as i32).sum()
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub enum HandRank {
    HighCard = 100, OnePair = 200, TwoPair = 300, ThreeOfAKind = 400, Straight = 500, Flush = 600,
    FullHouse = 700, FourOfAKind = 800, StraightFlush = 900,
}

impl HandRank {
    pub fn as_i32(&self) -> i32 {
        *self as i32
    }
}

#[derive(PartialEq, Eq)]
pub struct RankedHand {
    hand_rank: HandRank,
    combination_score: i32,
}


impl RankedHand {
    pub fn new(hand: Hand) -> RankedHand {
        let mut suit_hm = HashMap::new();
        let mut rank_hm = HashMap::new();
        for card in hand.hand.iter() {
            *suit_hm.entry(card.suit.clone()).or_insert(0) += 1;
            *rank_hm.entry(card.rank.clone()).or_insert(0) += 1;
        };
        let ranked = RankedHand::from_hand(&hand, &suit_hm, &rank_hm).unwrap();
        RankedHand {
            hand_rank: ranked.clone(),
            combination_score: RankedHand::hand_score_comb(&hand, &ranked, &rank_hm),
        }
    }

    pub fn hand_score_comb (hand: &Hand, hand_rank: &HandRank, rank_hm: &HashMap<Rank, i32>) -> i32 {
        match hand_rank {
            straight @ StraightFlush|straight @ Straight => return
                if hand.sum() == 28i32 { 15i32 } else { hand.sum() * straight.as_i32() },
            Flush => return hand.sum() * Flush.as_i32(),
            HighCard => return HighCard.as_i32()
                * hand.hand.last().unwrap().rank.into_i32()
                + hand.sum()
                - hand.hand.last().unwrap().rank.into_i32(),
            _ => rank_hm.iter().map(
                |(rank, &val)| rank.into_i32().clone()
                    * if val == 1 {
                    1
                } else if hand_rank == &FullHouse && val == 2 {
                    hand_rank.as_i32() / 2
                } else {
                    hand_rank.as_i32()
                }
            ).sum::<i32>()
        }
    }

    fn from_hand(hand: &Hand, suit_hm: &HashMap<Suit, i32>, rank_hm: &HashMap<Rank, i32>) -> Result<HandRank, ()> {
        match RankedHand::check_straight_flush(suit_hm, &hand) {
            found @ Ok(_) => return Ok(found.unwrap()),
            Err(_) => ()
        };
        match RankedHand::check_four_of_a_kind(rank_hm) {
            found @ Ok(_) => return Ok(
                found.unwrap()),
            Err(_) => ()
        };
        match RankedHand::check_straight(&hand) {
            found_straight @ Ok(_) => return Ok(found_straight.unwrap()),
            Err(_) => ()
        };
        match RankedHand::check_combination_high_card() {
            found_high@ Ok(_) => Ok(found_high.unwrap()),
            Err(_) => Err(())
        }
    }

    fn check_straight_flush(suit_hm: &HashMap<Suit, i32>, hand: &Hand) -> Result<HandRank, ()> {
        match suit_hm.iter()
            .map(|(_rank, &count)| count)
            .max()
            .unwrap() {
            5 => match RankedHand::check_straight(&hand) {
                Ok(_) => Ok(StraightFlush),
                Err(_) => Ok(Flush)
            },
            _ => Err(())
        }
    }

    fn check_four_of_a_kind(rank_hm: &HashMap<Rank, i32>) -> Result<HandRank, ()> {
        match rank_hm.iter().map(|(_rank, &count)| count).max().unwrap() {
            4 => Ok(FourOfAKind),
            3 => match RankedHand::check_full_house(rank_hm) {
                Ok(_) => Ok(FullHouse),
                Err(_) => Ok(ThreeOfAKind)
            },
            2 => match RankedHand::check_for_pairs(rank_hm) {
                pair_found @ Ok(_) => pair_found,
                Err(_) => Err(())
            },
            _ => Err(())
        }
    }

    fn check_full_house(rank_hm: &HashMap<Rank, i32>) -> Result<HandRank, ()> {
        match &rank_hm.iter().map(|(_rank, &count)| count).collect::<Vec<i32>>()[..] {
            [2i32, 3i32] | [3i32, 2i32]  => Ok(FullHouse),
            _ => Err(())
        }
    }

    fn check_straight(hand: &Hand) -> Result<HandRank, ()> {
        let rank_in_i32 = &hand.hand.last().unwrap().rank.into_i32();
        let expect_hand_sum = 5*((rank_in_i32 + rank_in_i32-4)/2);
        let actual_hand_sum = hand.sum();
        match actual_hand_sum {
            28i32 if hand.hand.iter().find(
                |cards| cards.rank == ACE) != None =>
                Ok(Straight),
            _ if actual_hand_sum == expect_hand_sum => Ok(Straight),
            _ => Err(())
        }
    }

    fn check_for_pairs(rank_hm: &HashMap<Rank, i32>) -> Result<HandRank, ()> {
        match rank_hm.iter().filter(|(_rank, &val)| val == 2 ).count() {
            2 => Ok(TwoPair),
            1 => Ok(OnePair),
            _ => Err(())
        }
    }

    fn check_combination_high_card() -> Result<HandRank, ()> {
        Ok(HighCard)
    }
}

impl PartialOrd for RankedHand{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for RankedHand {
    fn cmp(&self, other: &Self) -> Ordering {
        self.hand_rank.cmp(&other.hand_rank)
            .then(self.combination_score.cmp(&(other.combination_score)))
    }
}

pub fn winning_hands<'a>(hands: &[&'a str]) -> Vec<&'a str> {
    if hands.len() == 1 {
        return vec![hands[0]]
    }
    let mut hands_vec: Vec<(RankedHand, &'a str)> = vec![];
    for hand in hands.iter() {
        hands_vec.push(
            (
                RankedHand::new(
                    Hand {
                        hand: {
                            hand.split(' '
                            ).map(
                                |raw_card| Card::from_tuple_two_str(
                                    raw_card.split_at(raw_card.len()-1)
                                )
                            ).collect()
                        }
                    }.sort()
                ),
                hand
            )
        );
    };
    hands_vec.sort();
    let max_hand_vec = hands_vec.iter().max().unwrap().0.combination_score.clone();
    hands_vec.iter().filter(
        |&hands| hands.0.combination_score == max_hand_vec
    ).map(|winners| winners.1).collect()
}
