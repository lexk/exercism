/// Recursive Extended Euclidean algorithm d=MMI(x,y) & x, where ax + by = d
pub fn extended_euclid(a: i32, b: i32) -> (i32, i32, i32){
    if b == 0 { return (a, 1, 0)};
    let (d, x, y) = extended_euclid(b, a % b);
    (d, y, x - (a / b) * y)
}

/// non-recursive greatest common divisor
pub fn gcd(a: i32, b: i32) -> i32 {
    let mut ga = a.max(b);
    let mut gb = b.min(a);
    let mut r: i32;
    if ga == 0 {return gb};
    while gb != 0 { if ga > gb { r = ga % gb; } else { r = gb % ga; } ga = gb; gb = r; };
    ga
}

use crate::AffineCipherError::NotCoprime;
use crate::CryptType::{Decrypt, Encrypt};

const PLAIN: [char;26] = [
    'a','b','c','d','e','f','g','h','i','j','k','l', 'm',
    'n','o','p','q','r','s','t','u','v','w','x','y','z'
];

/// While the problem description indicates a return status of 1 should be returned on errors,
/// it is much more common to return a `Result`, so we provide an error type for the result here.
#[derive(Debug, Eq, PartialEq)]
pub enum AffineCipherError {
    NotCoprime(i32),
}

pub enum CryptType { Encrypt, Decrypt }

/// En-/Decipher Processing part
pub fn cipher_proc(plain: &str, a: i32, b:i32, crypt_tp: CryptType) -> String {
    plain.chars()
        .map(|inp_char|
            if inp_char.is_alphabetic() && !inp_char.is_whitespace(){
                PLAIN[match {
                    match &crypt_tp {
                        Encrypt => {(PLAIN.iter().position(|pl_char|
                            &inp_char.to_lowercase().to_string() == &pl_char.to_string()
                        ).unwrap() as i32 * a + b ) % 26},
                        // TODO: gcd and replace 27
                        Decrypt=> {(extended_euclid(a, 26).1
                            * (PLAIN.iter().position(|pl_char| &inp_char == pl_char
                        ).unwrap() as i32 - b)
                        ) % 26}
                    }
                } {neg_pat @ -1000..=-1 => 26 + neg_pat, pos_pat @ _ => pos_pat} as usize
                ].to_string() }
            else if inp_char.is_numeric() || inp_char.is_whitespace() {inp_char.to_string() }
            else { "".to_string() }).collect::<Vec<String>>().join("")
}

/// Encodes the plaintext using the affine cipher with key (`a`, `b`). Note that, rather than
/// returning a return code, the more common convention in Rust is to return a `Result`.
pub fn encode(plaintext: &str, a: i32, b: i32) -> Result<String, AffineCipherError> {
    match gcd(a, 26) {
         1 => Ok({
            cipher_proc(
                plaintext.to_lowercase().replace(" ", "")
                    .chars()
                    .filter(|raw_symbol| !raw_symbol.is_ascii_punctuation())
                    .enumerate()
                    .flat_map(|(idx, symbol)|
                        if (idx + 1) % 5 == 0 { [Some(symbol), Some(' ')] } else { [Some(symbol), None] }
                    )
                    .filter(|option_element| option_element.is_some())
                    .map(|opt| opt.unwrap())
                    .collect::<String>()
                    .as_str()
                , a, b, Encrypt
            ).trim().to_string()
        }),
        _ => Err(NotCoprime(a))
    }
}

/// Decodes the ciphertext using the affine cipher with key (`a`, `b`). Note that, rather than
/// returning a return code, the more common convention in Rust is to return a `Result`.
pub fn decode(ciphertext: &str, a: i32, b: i32) -> Result<String, AffineCipherError> {
    match gcd(a, 26) {
        1 => Ok(cipher_proc(ciphertext.replace(" ", "").as_str(),
                            a, b, Decrypt)),
        _ => Err(NotCoprime(a))
    }
}