use std::ops::Add;
use rand::Rng;

const ALPHABET_LEN: u8 = 26;
const RND_KEY_LEN: usize = 100;

pub enum CipherProc { Encode, Decode }

pub fn encode(key: &str, s: &str) -> Option<String> { proc_cipher(key, s, CipherProc::Encode) }

pub fn decode(key: &str, s: &str) -> Option<String> { proc_cipher(key, s, CipherProc::Decode) }

/// Generates random key with only a-z chars and encode {s}. Return tuple (key, encoded s)
pub fn encode_random(s: &str) -> (String, String) {
    let key_rnd = [0; RND_KEY_LEN].iter()
        .fold("".to_string(), |random_lol_str, _i| random_lol_str.add(
            (rand::thread_rng().gen_range(b'a'..=b'z') as char)
            .to_string().as_str())
        );
    let encoded = proc_cipher(key_rnd.as_str(),s, CipherProc::Encode).unwrap();
    (key_rnd, encoded)
}

/// Cipher process for {s} and {key}, distinguishing each flow. Return Option with/without cipher text
pub fn proc_cipher(key: &str, s: &str, cipher_op: CipherProc) -> Option<String> {
    match key.chars().all(|raw_char|
        raw_char.is_alphabetic() && raw_char.is_lowercase()) && !key.is_empty() && !s.is_empty() {
        true => Some(
            s.chars().enumerate().map(|plain_ch| {
                key.chars().enumerate()
                    .filter(|&key_ch| {
                        if key.len() <= s.len() && plain_ch.0 >= key.len() {
                            key_ch.0 == plain_ch.0 - key.len() }
                        else { key_ch.0 == plain_ch.0 }})
                    .map(|key_ch| ((
                        match cipher_op {
                            CipherProc::Encode =>
                                    b'a' + (plain_ch.1 as u8  + key_ch.1 as u8) % b'a' % ALPHABET_LEN,
                            CipherProc::Decode =>
                                if plain_ch.1 >= key_ch.1 {
                                    b'a' + (plain_ch.1.max(key_ch.1) as u8
                                    - key_ch.1.min(plain_ch.1) as u8) % b'a' % ALPHABET_LEN }
                                else {
                                    b'z' - (plain_ch.1.max(key_ch.1) as u8
                                    - key_ch.1.min(plain_ch.1) as u8) % b'a' % ALPHABET_LEN + 1 }
                        } ) as char ).to_string())
                    .collect::<String>() })
                .collect::<Vec<String>>().join("")
        ),
        false => None
    }
}