pub fn build_proverb(list: &[&str]) -> String {
    if list.len() > 0 {
        list.windows(2).fold(
            String::new(),
            |mut rhyme, proverbs| {
                rhyme += &*format!(
                    "For want of a {} the {} was lost.\n",
                    proverbs[0],
                    proverbs[1]
                );
                rhyme
            }
        ) + &*format!("And all for the want of a {}.", list.first().unwrap_or(&""))
    } else { "".to_string() }
}