use std::sync::RwLock;
use std::thread;
use std::collections::HashMap;
use std::sync::Arc;
pub fn frequency(input: &[&str], worker_count: usize) -> HashMap<char, usize> {
    let rw_lock_result = RwLock::new(HashMap::new());
    let arc_rwl_result = Arc::<RwLock<HashMap<char,usize>>>::from(rw_lock_result);
    let counter = |chunk: &[&str], threads_arc_rwl_result: Arc<RwLock<HashMap<char,usize>>>| {
        for line in chunk {
            for ch in line
                .chars()
                .filter(|ch| ch.is_alphabetic())
                .map(|ch| ch.to_ascii_lowercase()) {
                    *threads_arc_rwl_result.write().unwrap().entry(ch).or_insert(0) += 1;
            }
        };
    };
    match input.len() {
        0 => HashMap::<char, usize>::new(),
        k if k < 200 => {
            counter(input, arc_rwl_result.clone());
            RwLock::into_inner(Arc::try_unwrap(arc_rwl_result).unwrap()).unwrap()
        },
        _  => {
            let workers_count =
                if input.len() >= worker_count { worker_count }
                else { input.len() };
            let chunks_step = input.len() / workers_count;
            let input_chunks = input.chunks(chunks_step);
            thread::scope(
                |scope| {
                    for chunk in input_chunks {
                        let threads_arc_rwl_result = arc_rwl_result.clone();
                        scope
                            .spawn(
                                move || {
                                    counter(chunk, threads_arc_rwl_result)
                                }
                            )
                            .join()
                            .unwrap();
                    }
                    RwLock::into_inner(Arc::try_unwrap(arc_rwl_result).unwrap()).unwrap()
                }
            )
        }
    }
}