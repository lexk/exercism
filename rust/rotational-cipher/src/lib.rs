const PLAIN: [char;26] = [
    'a','b','c','d','e','f','g','h','i','j','k','l','m',
    'n','o','p','q','r','s','t','u','v','w','x','y','z'
];

pub fn rotate(input: &str, key: i8) -> String {
    let mut cipher_abc = PLAIN;
    if key > 0 { cipher_abc.rotate_left(key as usize);
    } else { cipher_abc.rotate_right(-key as usize); }
    input.chars().map(|ch|
        if ch.is_alphabetic() {
            if ch.is_ascii_uppercase() {
                cipher_abc[
                    PLAIN.iter()
                        .position(
                            |cipher_ch|cipher_ch.to_uppercase().to_string() == ch.to_string()
                        )
                        .unwrap()
                    ].to_uppercase().to_string()
            } else {
                cipher_abc[
                    PLAIN.iter()
                        .position(|cipher_ch|cipher_ch == &ch)
                        .unwrap()
                    ].to_string()
            }
        } else { ch.to_string() }
    )
        .collect::<Vec<String>>()
        .join("")
}