pub fn nth(n: u32) -> u32 {

    let mut iter_n:u32 = 1;
    let mut primes:Vec<u32> = Vec::new();

    'primes_add: while (primes.len() as u32) <= n {
        iter_n +=1;
        'subtract: for each in 2..iter_n {
            match iter_n % each {
                0 => continue 'primes_add,
                _ => continue 'subtract
            }
        }
        primes.push(iter_n);
    };
    primes[n as usize]
}
