pub fn reply(message: &str) -> &str {
    match (
        message.trim().chars().last().unwrap_or('0') == '?',
        (
            message.chars().filter(|ch| ch.is_alphabetic())
                .all(|raw_ch| raw_ch.is_uppercase())
            && !(message.chars().filter(|ch| ch.is_alphabetic()).count() < 1)
        ),
        (message.chars().all(|v|v.is_whitespace() || message.is_empty()))
    )  {
        (true, false, _) => "Sure.",
        (false, true, false) => "Whoa, chill out!",
        (true, true, false) => "Calm down, I know what I'm doing!",
        (_, _, true) => "Fine. Be that way!",
        (_, _, false) => "Whatever."
    }
}
