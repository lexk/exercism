use std::collections::BTreeMap;

pub fn actions(n: u8) -> Vec<&'static str> {
    let reverse = 0b10000;
    let actions_map = BTreeMap::from([
        (0b00001, "wink"),
        (0b00010, "double blink"),
        (0b00100, "close your eyes"),
        (0b01000, "jump"),
    ]);

    let res = actions_map
        .iter()
        .filter(|action| action.0 & n > 0)
        .map(|action| *action.1);

    if n & reverse > 0 { res.rev().collect() } else { res.collect() }
}