pub fn abbreviate(phrase: &str) -> String {
    if phrase.is_empty() { "".to_string() } else {
        phrase
            .split_terminator(&[' ', '-', '_'][..])
            .filter(|words| !words.is_empty())
            .map(|word|
                if word[1..].contains(|sym_upp: char| sym_upp.is_uppercase())
                    && word.contains(|sym_low: char| sym_low.is_lowercase()) {
                    word.chars().filter(|ch| ch.is_uppercase()).collect::<String>()
                }
                else { word.chars().nth(0).unwrap_or(' ').to_uppercase().to_string() }
            )
            .collect::<String>()
    }
}