use std::collections::HashMap;

/// Count occurrences of words.
pub fn word_count(words: &str) -> HashMap<String, u32> {
    words.to_lowercase().split(|delim: char| delim == ',' || delim.is_whitespace())
            .map(|word_raw| word_raw.chars()
            .filter_map(|ch| if ch.is_alphanumeric() || ch == '\'' { Some(ch)} else { None })
            .collect::<String>()
            .trim_matches(|letter: char| !letter.is_alphanumeric()).to_owned())
        .filter(|word_cleared| !word_cleared.is_empty())
        .fold(HashMap::<String, u32>::new(), |mut result_hm, r|
            { *result_hm.entry(r).or_insert(0) += 1; result_hm } )
}