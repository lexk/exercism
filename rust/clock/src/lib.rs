use std::fmt;
use std::fmt::Formatter;

#[derive(Debug)]
pub struct Clock {
    hours: i32,
    minutes: i32
}

impl Clock {

    pub fn new(hours: i32, minutes: i32) -> Self {

        let mut new_min = minutes;
        let mut new_hour = hours;

        match new_min {
            0..=59 => {},
            -60..=-1 => {
                new_min += 60;
                new_hour -= 1},
            _ => {
                new_hour += new_min/60;
                new_min -= 60*(new_min/60);
                if (-60..=-1).contains(&new_min) {
                    new_min += 60;
                    new_hour -= 1};}
        }

        match new_hour {
            0..=23 => new_hour +=0,
            -24..=-1 => new_hour += 24,
            _ => {
                new_hour -= 24*(new_hour/24);
                if (-24..=-1).contains(&new_hour) {
                    new_hour += 24}}
        }

        Clock { hours: new_hour, minutes: new_min }
    }

    pub fn add_minutes(&self, minutes: i32) -> Self {
        let new_min = minutes + self.minutes;
        let new_hour = self.hours;
        Clock::new(new_hour, new_min)
    }

    pub fn to_string(&self) -> String {
        match (self.hours, self.minutes) {
            (0..=9, 0..=9) => format!("0{}:0{}", self.hours, self.minutes),
            (0..=9, _) => format!("0{}:{}", self.hours, self.minutes),
            (_, 0..=9) => format!("{}:0{}", self.hours, self.minutes),
            (_,_) => format!("{}:{}", self.hours, self.minutes)
        }
    }
}
impl PartialEq for Clock {
    fn eq(&self, rhs: &Self) -> bool {
        self.hours == rhs.hours && self.minutes == rhs.minutes
    }
}

impl fmt::Display for Clock {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:02}:{:02}", self.hours, self.minutes)
    }
}
