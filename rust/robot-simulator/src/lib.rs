// The code below is a stub. Just enough to satisfy the compiler.
// In order to pass the tests you can add-to or change any of this code.

use crate::Direction::{North, East, South, West};

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
pub enum Direction { North, East, South, West, }

const DIRECTION: [Direction; 4] = [North, East, South, West];

#[derive(Copy, Clone)]
pub struct Robot { x: i32, y: i32, d: Direction }

impl Robot {
    pub fn new(x: i32, y: i32, d: Direction) -> Self {
        Robot{x, y, d}
    }

    #[must_use]
    pub fn turn_right(&self) -> Self {
        let position = DIRECTION.iter().position(|dir| dir == &self.d).unwrap();
        let mut new_direction = DIRECTION.clone();
        new_direction.rotate_left(1);
        Robot::new(self.x, self.y, new_direction[position].clone())
    }

    #[must_use]
    pub fn turn_left(&self) -> Self {
        let position = DIRECTION.iter().position(|dir| dir == &self.d).unwrap();
        let mut new_direction = DIRECTION.clone();
        new_direction.rotate_right(1);
        Robot::new( self.x, self.y,new_direction[position].clone())
    }

    #[must_use]
    pub fn advance(&self) -> Self {
        let advance = match self.d {
            North => (0, 1),
            East => (1, 0),
            South => (0, -1),
            West => (-1, 0),
        };
        Robot::new(self.x + advance.0,self.y + advance.1, self.d)
    }

    #[must_use]
    pub fn instructions(mut self, instructions: &str) -> Self {
        let _pp: () = instructions.chars().map(|action|
            self = match action {
                'R' => self.turn_right(),
                'L' => self.turn_left(),
                'A' => self.advance(),
                _ => self
            }
        ).collect();
        self
    }

    pub fn position(&self) -> (i32, i32) {
        (self.x, self.y)
    }

    pub fn direction(&self) -> &Direction {
        &self.d
    }
}