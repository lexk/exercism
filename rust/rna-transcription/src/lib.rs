#[derive(Debug, PartialEq, Eq)]
pub struct Dna {nuc: String}

#[derive(Debug, PartialEq, Eq)]
pub struct Rna { nuc: String}

impl Dna {
    pub fn new(dna: &str) -> Result<Dna, usize> {
        match dna.chars().enumerate().find(|nuc| nuc.1 == 'U' 
            || (nuc.1 !='G' && nuc.1 !='C' && nuc.1 !='T' && nuc.1 !='A')) 
        { Some(nuc) => Err(nuc.0), None => Ok(Dna {nuc: dna.to_string()}) }
    }
    pub fn into_rna(self) -> Rna {
        Rna { nuc: self.nuc.chars().map(|nuc_ch| match nuc_ch { 'G' => "C", 'C' => "G",
            'T' => "A", 'A' => "U", _ => "" }).map(|el| el.to_string())
            .collect::<Vec<String>>().join("")}
    }
}

impl Rna {
    pub fn new(rna: &str) -> Result<Rna, usize> {
        match rna.chars().enumerate().find(|nuc| nuc.1 == 'T'
            || (nuc.1 !='G' && nuc.1 !='C' && nuc.1 !='U' && nuc.1 !='A'))
        { Some(nuc) => Err(nuc.0), None => Ok(Rna {nuc: rna.to_string()}) }
    }
}