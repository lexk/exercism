use std::collections::{HashMap, HashSet};
const NUCLEOTIDES: [char; 4] = ['A', 'C', 'G', 'T'];

pub fn count(nucleotide: char, dna: &str) -> Result<usize, char> {
    if !NUCLEOTIDES.contains(&nucleotide) { return Err(nucleotide) }
    match dna
        .chars()
        .map(|raw_nuc| (!NUCLEOTIDES.contains(&raw_nuc), raw_nuc))
        .filter(|checked_false_nuc| checked_false_nuc.0 == true)
        .collect::<Vec<(bool, char)>>() {
        broken_chain_nucs @ _ if !broken_chain_nucs.is_empty() => Err(broken_chain_nucs[0].1),
        _ => Ok(dna.chars().filter(|raw_nuc| raw_nuc == &nucleotide).count())
    }
}

pub fn nucleotide_counts(dna: &str) -> Result<HashMap<char, usize>, char> {
    let mut nuc_hm = HashMap::<char, usize>::new();
    NUCLEOTIDES.iter()
        .map(|&nuc| nuc_hm.insert(nuc, 0))
        .for_each(drop);
    if dna.is_empty() { Ok(nuc_hm) }
    else {
        dna.chars()
            .map(|raw_nuc| *nuc_hm.entry(raw_nuc).or_insert(1)  += 1)
            .for_each(drop);
        match &(nuc_hm.keys().into_iter().cloned().collect::<HashSet<char>>())
            - &HashSet::from(NUCLEOTIDES) {
            diff_empty @ _ if diff_empty.is_empty() => Ok(nuc_hm),
            diff_not_empty @ _ => Err(diff_not_empty.iter().cloned().last().unwrap())
        }
    }
}