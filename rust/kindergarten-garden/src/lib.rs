use std::collections::HashMap;

const CHILDREN: [&'static str; 12] = ["Alice", "Bob", "Charlie", "David", "Eve", "Fred", "Ginny",
    "Harriet", "Ileana", "Joseph", "Kincaid", "Larry"];

pub fn plants(diagram: &str, student: &str) -> Vec<&'static str> {
    let plants: HashMap<char, &'static str> = HashMap::from([
        ('G', "grass"),
        ('C', "clover"),
        ('R', "radishes"),
        ('V', "violets")
    ]);
    let name_idx =
        CHILDREN
            .iter()
            .position(|name| name == &student)
            .unwrap();
    diagram
        .split("\n")
        .fold(Vec::<&'static str>::new(), |mut result, row| {
            result.append(&mut vec![
                plants.get(&row.chars().nth(name_idx*2).unwrap()).unwrap().clone(),
                plants.get(&row.chars().nth(name_idx*2+1).unwrap()).unwrap().clone()
            ]);
            result
        }
        )
}
