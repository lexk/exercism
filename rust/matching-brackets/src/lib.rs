use crate::BracketsSide::{Left, Right};
use crate::BracketsType::{Braces, Brackets, Parentheses};

#[derive(Eq, PartialEq, Clone, Copy)]
pub enum BracketsType { Brackets, Braces, Parentheses }

#[derive(Eq, PartialEq, Clone, Copy)]
pub enum BracketsSide { Left, Right }

impl BracketsSide {
    pub fn is_opposite(&self, other: BracketsSide) -> bool {
        self != &other
    }
}

#[derive(Clone, Copy)]
pub struct Bracket { b_type: BracketsType, b_side: BracketsSide }

impl Bracket {
    pub fn from_char(c: &char) -> Option<Bracket> {
        match c {
            '[' => Some(Bracket { b_type: Brackets, b_side: Left }),
            ']' => Some(Bracket { b_type: Brackets, b_side: Right }),
            '(' => Some(Bracket { b_type: Braces, b_side: Left }),
            ')' => Some(Bracket { b_type: Braces, b_side: Right }),
            '{' => Some(Bracket { b_type: Parentheses, b_side: Left }),
            '}' => Some(Bracket { b_type: Parentheses, b_side: Right }),
            _ => None
        }
    }

    pub fn is_pair(&self, other: &Bracket) -> bool {
        self.b_type == other.b_type && (self.b_side.clone(), other.b_side.clone()) == (Left, Right)
    }
}

pub fn brackets_are_balanced(string: &str) -> bool {
    let mut stack = Vec::new();
    for x in string.chars() {
        match Bracket::from_char(&x) {
            Some(br) if br.b_side == Left => stack.push(br),
            Some(br) if br.b_side == Right && !stack.pop().unwrap_or(br).is_pair(&br) => {return false}
            _ => (),
        }
    }
    stack.is_empty()
}