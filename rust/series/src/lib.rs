pub fn series(digits: &str, len: usize) -> Vec<String> {
    if len < 1 { return vec![ "".to_owned(); digits.chars().count() +1 ] }
    digits
        .chars()
        .collect::<Vec<char>>()
        .windows(len)
        .map(|chars| chars.into_iter().collect::<String>())
        .collect()
}
