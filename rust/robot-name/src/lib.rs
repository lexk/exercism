use std::cell::Cell;

thread_local!(static ROBOT_ID: Cell<usize> = Cell::new(0));

pub struct Robot {
    name: String,
}

impl Robot {
    pub fn new() -> Self {
        let mut robot = Robot {name: String::new()};
        let _ = &robot.reset_name();
        robot
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    fn id_to_name(id: usize) -> String {
        match id / 999 {
            thousands @ 0..=575 => {
                let mut name = ((thousands / 24 % 65 + 65) as u8 as char).to_string();
                name.push((thousands % 24 + 65) as u8 as char);
                name + format!("{:03}", id % 999).as_str()
            }
            _ => {
                println!("We are out of robot names, change the convention!");
                "".to_string()
            }
        }
    }

    pub fn reset_name(&mut self) {
        ROBOT_ID.with(|thread_id| {
            let id = thread_id.get();
            thread_id.set(id + 1);
            self.name = Self::id_to_name(id);})
    }
}