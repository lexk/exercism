use std::collections::HashMap;

#[allow(clippy::new_without_default)]
pub struct School {grades: HashMap<u32, Vec<String>>}
// TODO: BTreeMap instead of HashMap
impl School {
    pub fn new() -> School { School {grades: HashMap::<u32, Vec<String>>::new()} }

    pub fn add(&mut self, grade: u32, student: &str) {
        self.grades.entry(grade).and_modify(|students|
            students.push(student.to_owned())
        ).or_insert(vec![student.to_string()]);
    }

    pub fn grades(&self) -> Vec<u32> {
        match self.grades
            .keys()
            .map(|&key_grade| key_grade)
            .collect::<Vec<u32>>() {
            empty_vec @ _ if empty_vec.is_empty() => vec![],
            mut valid_vec @ _ => { valid_vec.sort(); valid_vec }
        }
    }

    pub fn grade(&self, grade: u32) -> Vec<String> {
       match self.grades.contains_key(&grade) {
           true => match self.grades[&grade].clone() {
               mut valid_grade @ _ => { valid_grade.sort(); valid_grade }
           },
           false => vec![],
       }
    }
}
