const DROPS: [(u32, &'static str); 3] = [(3, "Pling"), (5, "Plang"), (7, "Plong")];

pub fn raindrops(n: u32) -> String {
    match DROPS.iter()
        .filter_map(|div| {if n % div.0 == 0 { Some(div.1) } else { None }} )
        .collect::<String>() {
        result_empty @ _ if result_empty.is_empty() => n.to_string(),
        result_str @ _ => result_str
    }
}