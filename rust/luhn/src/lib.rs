/// Check a Luhn checksum.
pub fn is_valid(code: &str) -> bool {
    match code.trim().chars().rev()
        .filter(|ch| !ch.is_whitespace())
        .map(|char_dig| char::to_digit(char_dig, 10))
        .map(|char_dig_opt|
            if char_dig_opt.is_some() { char_dig_opt.unwrap()} else { 9u32 }
        )
        .enumerate()
        .map(|(idx, digit) |
            if (idx+1) % 2 == 0 {
                if digit > 4u32 { (digit * 2) - 9 }
                else { digit * 2 }
            } else { digit }
        )
        .sum::<u32>() {
        (0..=9) => if code.contains("00") { true } else { false },
        check_res @ _ => if check_res % 10 == 0 { true } else { false }
    }
}