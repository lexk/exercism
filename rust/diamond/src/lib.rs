pub fn get_diamond(c: char) -> Vec<String> {
    ('A'..=c)
        .into_iter()
        .enumerate()
        .fold(
            Vec::<String>::new(), | mut result_vec, raw_ch| {
                if raw_ch.0 == 0 {
                    ('A'..=c)
                        .for_each(|_d|result_vec.push(" ".repeat(c as usize - 'A' as usize)))
                };
                result_vec[raw_ch.0].insert(raw_ch.0, raw_ch.1);
                result_vec[raw_ch.0] =
                    result_vec[raw_ch.0][1..]
                        .chars()
                        .rev()
                        .collect::<String>()
                    + result_vec[raw_ch.0].as_str();
                if raw_ch.1 == c { result_vec
                    .append(
                        &mut result_vec[..result_vec.len()-1]
                            .iter()
                            .rev()
                            .cloned()
                            .collect::<Vec<String>>()
                    )
                };
                result_vec
            }
        )
}