use std::cmp::Ordering;
use std::collections::HashMap;
use crate::GameResult::{DRAW, LOSS, WIN};

const TABLE_HEADER: &str = "Team                           | MP |  W |  D |  L |  P";

#[derive(PartialEq, Eq, Copy, Clone)]
pub enum GameResult { WIN = 3, DRAW = 1, LOSS = 0, }

impl GameResult {
    pub fn from_str(result_str: &str) -> Result<GameResult, ()> {
        match result_str {
            "win" => Ok(WIN),
            "draw" => Ok(DRAW),
            "loss" => Ok(LOSS),
            _ => Err(())
        }
    }
    pub fn into_i32(self) -> i32 {
        self as i32
    }
    pub fn opposite(self) -> GameResult {
        match self { WIN => LOSS, LOSS => WIN, DRAW => DRAW }
    }
}

#[derive(Clone)]
pub struct TeamResult {
    team_name: String,
    result: GameResult
}

#[derive(PartialEq, Eq)]
pub struct TeamScore {
    name: String,
    matches_played: usize,
    matches_won: usize,
    matches_drawn: usize,
    matches_loss: usize,
    points: i32
}

impl Ord for TeamScore {
    fn cmp(&self, other: &Self) -> Ordering {
        self.points.cmp(&other.points)
    }
}

impl PartialOrd<Self> for TeamScore {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other).then(self.name.cmp(&(other.name)).reverse()))
    }
}

impl TeamScore {
    pub fn from_hashmap(teams_name: String, game_results: &Vec<GameResult>) -> TeamScore {
        TeamScore {
            name: teams_name,
            matches_played: game_results.len().into(),
            matches_won: game_results.iter()
                .filter(|&matches| matches == &WIN).count().into(),
            matches_drawn: game_results.iter()
                .filter(|&matches| matches == &DRAW).count().into(),
            matches_loss: game_results.iter()
                .filter(|&matches| matches == &LOSS).count().into(),
            points: game_results.iter().map(|result| result.clone().into_i32()).sum()
        }
    }
    pub fn to_string(&self) -> String {
        format!("{:<30} | {:>2} | {:>2} | {:>2} | {:>2} | {:>2}",
                self.name, self.matches_played, self.matches_won, self.matches_drawn,
                self.matches_loss, self.points
        )
    }
}

pub struct Game { result: Vec<TeamResult> }

impl Game {
    pub fn from_str(game_str: &str) -> Result<Game, ()> {
        let game_result: Vec<&str> = game_str.split(';').collect();
        Ok(Game{ result: vec![
            TeamResult{
                team_name: game_result.first().unwrap().to_string(),
                result: GameResult::from_str(game_result[2]
                    .clone()).unwrap()
            },
            TeamResult{
                team_name: game_result[1].to_string(),
                result: GameResult::from_str(game_result[2]
                    .clone()).unwrap().opposite()
            }
        ]})
    }
}

pub struct Tournament {
    teams_scores: Vec<TeamScore>
}

impl Tournament {
    pub fn new(tournament_string: String) -> Tournament {
        Tournament {
            teams_scores: Tournament::game_vec_into_hm(tournament_string.split('\n')
                .map(|game| Game::from_str(game).unwrap())
                .collect::<Vec<Game>>())
                .iter().map(|(name, result_vec)|
                TeamScore::from_hashmap(name.clone(), result_vec)).collect()
        }
    }
    pub fn into_table(mut self) -> String {
        self.teams_scores.sort();
        if self.teams_scores.len() < 1 {
            TABLE_HEADER.to_string()
        } else {
            TABLE_HEADER.to_string() + "\n"
                + &self.teams_scores.iter().rev().map(
                |record| record.to_string().to_owned()
            ).collect::<Vec<String>>().join("\n")
        }
    }
    pub fn game_vec_into_hm(game_vec: Vec<Game>) -> HashMap<String, Vec<GameResult>> {
        let mut games_hm: HashMap<String, Vec<GameResult>> = HashMap::new();
        for game in game_vec.iter(){
            for team_res in &game.result{
                games_hm.entry(team_res.team_name.clone())
                    .or_insert(vec![])
                    .push(team_res.result.clone())
            }
        }
        games_hm
    }
}

pub fn tally(match_results: &str) -> String {
    Tournament::into_table(match match_results {
        "" => Tournament {teams_scores: vec![]},
        _ => Tournament::new(match_results.to_string())
    })
}