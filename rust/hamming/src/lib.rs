/// Return the Hamming distance between the strings,
/// or None if the lengths are mismatched.
pub fn hamming_distance(s1: &str, s2: &str) -> Option<usize> {
    match s1 == s2 {
        true => { Some(0) }
        false => {
            match s1.len() == s2.len() {
                false => { None }
                true => {
                    Some(
                        s1.chars().zip(s2.chars())
                            .fold(0usize, | count, (s1_i, s2_i)| {
                                if s1_i != s2_i { count + 1 }
                                else { count } }
                            )
                    )
                }
            }
        }
    }
}