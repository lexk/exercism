pub fn is_armstrong_number(num: u32) -> bool {

    let numb_pow = ((num as f32).log10() +1.).floor() as u32;
    (0..numb_pow)
        .map(|f| (num / 10u32.pow(f) % 10).pow(numb_pow))
        .sum::<u32>() == num
}
