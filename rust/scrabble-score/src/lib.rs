/// Compute the Scrabble score for a word.
pub fn score(word: &str) -> u64 {
    word
        .to_lowercase()
        .chars()
        .fold(0u64, |result, letter| {
            result + match letter {
                'a' | 'e' | 'i' | 'o' | 'u' | 'l' | 'n' | 'r' | 's' | 't'   => 1u64,
                'd' | 'g'                                                   => 2u64,
                'b' | 'c' | 'm' | 'p'                                       => 3u64,
                'f' | 'h' | 'v' | 'w' | 'y'                                 => 4u64,
                'k'                                                         => 5u64,
                'j' | 'x'                                                   => 8u64,
                'q' | 'z'                                                   => 10u64,
                _                                                           => 0u64,
            }
        })
}