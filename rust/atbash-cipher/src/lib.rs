const PLAIN: [char;26] = [
    'a','b','c','d','e','f','g','h','i','j','k','l', 'm',
    'n','o','p','q','r','s','t','u','v','w','x','y','z'
];

/// Applies reverse alphabet order
pub fn cipher_proc(input: &str) -> String {
    input.chars().map(|ch|
        if ch.is_alphabetic() && !ch.is_whitespace() {
            PLAIN[
                25-PLAIN.iter()
                    .position(|cipher_ch|cipher_ch == &ch)
                    .unwrap()
                ].to_string()
        } else if ch.is_numeric() || ch.is_whitespace() {ch.to_string()
        } else { "".to_string() }
    )
        .collect::<Vec<String>>()
        .join("").trim().to_string()
}

/// "Decipher" with the Atbash cipher.
pub fn decode(cipher: &str) -> String {
    cipher_proc(cipher.replace(" ", "").as_str())
}

/// "Encipher" with the Atbash cipher.
pub fn encode(plain: &str) -> String {
    cipher_proc(plain.to_lowercase()
        .replace(" ", "")
        .chars()
        .filter(|raw_symbol| !raw_symbol.is_ascii_punctuation())
        .enumerate()
        .flat_map(|(idx,symbol)|
            if (idx + 1) % 5 == 0 { [Some(symbol), Some(' ')]} else { [Some(symbol), None] }
        )
        .filter(|option_element| option_element.is_some())
        .map(|opt| opt.unwrap())
        .collect::<String>()
        .as_str()
    )
}