pub fn encrypt(input: &str) -> String {
    let columns: usize = (input.chars().filter( |s| s.is_alphanumeric() )
        .count() as f64).sqrt().ceil() as usize;
    input.to_lowercase().chars()
        .filter( |raw_char| raw_char.is_alphanumeric() ).enumerate()
        .fold(Vec::<Vec<String>>::new(), |mut acc_vv_str, char_id| {
            if acc_vv_str.len() < columns { acc_vv_str.push(vec![])};
            acc_vv_str[char_id.0 % columns].push(char::to_string(&char_id.1));
            acc_vv_str
        }).iter()
        .map(|row_vec|
            match row_vec.join("").as_str() {
                "" => "".to_string(),
                row @ _ if columns > row_vec.len() => row.to_owned()
                    + " ".repeat( columns - row_vec.len() -1).as_str(),
                row @ _  => row.to_owned()
            })
        .collect::<Vec<String>>().join(" ")
}