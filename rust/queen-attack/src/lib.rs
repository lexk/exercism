#[derive(Debug)]
pub struct ChessPosition{
    rank: i8,
    file: i8
}

#[derive(Debug)]
pub struct Queen{
    position: ChessPosition,
}

impl ChessPosition {
    pub fn new(rank: i32, file: i32) -> Option<Self> {
        match [rank, file].iter().all(|point| point >= &0 && point < &8 ) {
            true => Some( ChessPosition { rank: rank as i8, file: file as i8 } ),
            false => None
        }
    }
}

impl Queen {
    pub fn new(position: ChessPosition) -> Self { Queen { position } }

    pub fn can_attack(&self, other: &Queen) -> bool {
        self.position.rank == other.position.rank
            || self.position.file == other.position.file
            || (self.position.file - other.position.file).abs() == (self.position.rank - other.position.rank).abs()
    }
}
