use std::collections::HashSet;

pub fn anagrams_for<'a>(word: &str, possible_anagrams: &[&'a str]) -> HashSet<&'a str> {
    let lower_word = word.to_lowercase();
    let low_word_v_sorted = into_sorted_lower_vec(lower_word.as_str());
    possible_anagrams.iter()
        .filter_map(|&possible_ana| {
            let low_possible_ana = possible_ana.to_lowercase();
            match into_sorted_lower_vec(low_possible_ana.as_str())
            {
                pos_v_sorted
                    if pos_v_sorted == low_word_v_sorted && low_possible_ana != lower_word
                => Ok(possible_ana),
                _ => Err(())
            }
        }.ok())
        .collect()
}

fn into_sorted_lower_vec(word: &str) -> Vec<char> {
    let mut word_vec = word.chars().collect::<Vec<char>>();
    word_vec.sort_unstable(); word_vec
}