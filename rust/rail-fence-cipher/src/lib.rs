pub struct RailFence{ rails: u32 }

impl RailFence {
    pub fn new(rails: u32) -> RailFence { RailFence{rails} }
    pub fn encode(&self, text: &str) -> String { self.cipher_order(text.chars()) }

    /// Creates cipher order sequence from iterator of chars for both encryption and decryption.
    /// For encryption no additional steps needed, for the decryption output will be used as a key
    fn cipher_order(&self, char_iter: impl Iterator<Item=char>) -> String {
        char_iter.enumerate()
            .fold(Vec::<String>::new(),|mut result_vec_str, order_symbol| {
                if result_vec_str.is_empty() { (1..=self.rails)
                    .for_each(|_| result_vec_str.push("".to_owned()));}
                if order_symbol.0 == 0 { result_vec_str[0].push(order_symbol.1)}
                else {
                    match ((order_symbol.0 ) / (self.rails -1 ) as usize ) % 2 {
                        0 => result_vec_str[order_symbol.0 % (self.rails -1) as usize]
                            .push(order_symbol.1),
                        _ => result_vec_str[(self.rails as usize -1) - ((order_symbol.0 ) % (self.rails-1) as usize)]
                            .push(order_symbol.1),
                    }
                }
                result_vec_str
            }).join("")
    }

    /// Decipher text based on cipher_order generated key
    pub fn decode(&self, cipher: &str) -> String {
        self.cipher_order((0 as char..=(cipher.len() as u8 -1) as char).into_iter())
            .chars()
            .enumerate()
            .fold(Vec::<String>::new(), |mut result_vec, order_symbol|
                {if result_vec.is_empty() {
                    (1..=cipher.len()).map(|_| result_vec.push(" ".to_owned())).for_each(drop)
                };
                    result_vec[order_symbol.1 as usize] = cipher.chars().nth(order_symbol.0).unwrap().to_string();
                    result_vec
                }
            ).join("")
    }
}