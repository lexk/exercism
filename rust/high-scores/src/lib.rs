#[derive(Debug)]
pub struct HighScores<'a> {
    scores: &'a [u32]
}

impl<'a> HighScores<'a> {
    pub fn new(scores: &'a [u32]) -> Self { Self{ scores } }

    pub fn scores(&self) -> &[u32] { self.scores }

    pub fn latest(&self) -> Option<u32> { self.scores.last().cloned() }

    pub fn personal_best(&self) -> Option<u32> { self.scores.iter().max().cloned() }

    pub fn personal_top_three(&self) -> Vec<u32> {
        if self.scores.is_empty() { vec![] }
        else {
            let mut top = self.scores.to_vec();
            top.sort();
            top.reverse();
            match self.scores.len() {
                low @ 0..=2 => top[..=(low-1)].to_vec(),
                3.. => top[..=2].to_vec(),
                _ => vec![]
            }
        }
    }
}
