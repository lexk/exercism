use std::collections::HashMap;
use crate::Error::{EmptyBuffer, FullBuffer};

pub struct CircularBuffer<T> {
    data: HashMap<usize, T>,
    capacity: usize,
    empty_space: usize,
    oldest_idx: usize
}

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    EmptyBuffer,
    FullBuffer,
}

impl<T> CircularBuffer<T> {
    pub fn new(capacity: usize) -> Self {
        CircularBuffer {
            data: HashMap::new(),
            capacity,
            empty_space: capacity,
            oldest_idx: 0
        }
    }

    pub fn write(&mut self, element: T) -> Result<(), Error> {
        match self.empty_space {
            0 => Err(FullBuffer),
            _ => {
                Ok({
                    self.data.insert(self.capacity - self.empty_space + self.oldest_idx, element);
                    self.empty_space -= 1;
                })
            }
        }
    }

    pub fn read(&mut self) -> Result<T, Error> {
        if self.empty_space == self.capacity {
            Err(EmptyBuffer)
        } else {
            Ok(
                {
                    let remove_idx = self.oldest_idx.clone();
                    if self.capacity > self.empty_space {
                        self.oldest_idx = self.oldest_idx + 1;
                    } else { self.oldest_idx = 0; }
                    self.empty_space += 1;
                    self.data.remove(&remove_idx).unwrap()
                }
            )

        }
    }

    pub fn clear(&mut self) {
        self.data.drain();
        self.data.reserve(self.capacity);
        self.oldest_idx = 0;
        self.empty_space = self.capacity;
    }

    pub fn overwrite(&mut self, element: T) {
        if self.empty_space == 0 {
            let _ = self.read();
        }
        let _ = self.write(element);
    }
}
