use std::collections::HashMap;
const ALPHABET: &str = "abcdefghijklmnopqrstuvwxyz";

/// Counting characters and in sentence and unifying 'a' to 'z' lower and upper
/// cases for counting purposes
fn into_hm(words: &str) -> HashMap<char, i32> {
    words
        .chars()
        .fold(
            HashMap::new(),
            |mut hm_letters_occur, letter| {

                *hm_letters_occur.entry(
                    if ('A'..='Z').contains(&letter) { (letter as u8 + 32) as char }
                    else { letter }
                ).or_insert(1) +=1;
                hm_letters_occur
            }
        )
}

/// Determine whether a sentence is a pangram.
pub fn is_pangram(sentence: &str) -> bool {
    let apl = into_hm(ALPHABET);
    let snet = into_hm(sentence);
    !apl
        .iter()
        .map(|k| k.1 <= snet.get(k.0).unwrap_or(&0))
        .collect::<Vec<bool>>()
        .contains(&false)
}

