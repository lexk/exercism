use std::cmp::Ordering;
use std::collections::{HashMap};

#[derive(Debug, PartialEq, Clone, Eq, Hash)]
pub struct Item {
    pub weight: u32,
    pub value: u32,
}

impl PartialOrd for Item {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.value.partial_cmp(&other.value).and(other.weight.partial_cmp(&self.weight))
    }
}

#[derive(Debug, PartialEq, Clone, Eq)]
pub struct Sack {
    items: HashMap<Item, u32>,
    value: u32
}

impl Sack {
    pub fn from_weight_items(max_weight: u32, items: &[Item]) -> Sack {
        let mut w_remain = max_weight.clone();
        let sack_items =
            items
                .iter()
                .cloned()
                .fold(
                    HashMap::<Item, u32>::new(),
                    |mut res, it| {
                        match w_remain.checked_sub(it.weight) {
                            None => {}
                            Some(rem) => {
                                *res.entry(it).or_insert(0) += 1;
                                w_remain=rem
                            }
                        }
                        res
                    }
                );
        Sack {
            items: sack_items.clone(),
            value: sack_items.iter().map(|(it, count)| it.value * count).sum()
        }
    }

}

impl Ord for Sack {
    fn cmp(&self, other: &Self) -> Ordering {
        self.value.cmp(&other.value)
    }
}

impl PartialOrd for Sack {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.value.partial_cmp(&other.value)
    }
}

pub fn maximum_value(max_weight: u32, items: &[Item]) -> u32 {
    let mut sorted_by_weight_items = items.to_vec();
    sorted_by_weight_items.sort_by(|a, b| b.weight.partial_cmp(&a.weight).and(a.value.partial_cmp(&b.value)).unwrap());
    // sorted_by_weight_items.reverse();
    let mut s_combinations = sack_combinations(max_weight, &sorted_by_weight_items[..]);
    s_combinations.sort();
    s_combinations.last().unwrap().value
    // todo!("calculate the maximum value achievable with the given {items:?} and {max_weight}");
}

pub fn sack_combinations(max_weight: u32, items: &[Item]) -> Vec<Sack> {
    items.iter()
        .enumerate()
        .map(|(idx, _item)| Sack::from_weight_items(max_weight, &items[idx..]))
        .collect()
}