pub fn verse(n: u32) -> String {
    let bottles = match n {
        0 => ("No more".to_string(),
              "Go to the store and buy some more".to_string(),
              99.to_string(),
              ("s".to_string(),"s".to_string(), "s".to_string())),
        1 => (n.to_string(),
              "Take it down and pass it around".to_string(),
              "No more".to_string(),
              ("".to_string(), "".to_string(), "s".to_string())),
        2 => (n.to_string(),
              "Take one down and pass it around".to_string(),
              1.to_string(),
              ("s".to_string(), "s".to_string(), "".to_string())),
        _ => (n.to_string(),
              "Take one down and pass it around".to_string(),
              (n-1).to_string(),
              ("s".to_string(),"s".to_string(), "s".to_string()))
    };
    format!("{} bottle{s1} of beer on the wall, {} bottle{s2} of beer.\n\
    {}, {} bottle{s3} of beer on the wall.\n",
            bottles.0, bottles.0.to_lowercase(), bottles.1, bottles.2.to_lowercase(),
            s1 = bottles.3.0, s2 = bottles.3.1, s3 = bottles.3.2)
}

pub fn sing(start: u32, end: u32) -> String {
    let (begin, finish, rev) =
        if start > end { ( end, start+1, true ) } else { ( start ,end, false ) };
    let song = (begin..finish).map(
        |n|
            verse(n)).collect::<Vec<String>>();
    if rev {
        let mut rev_song = song.clone(); rev_song.reverse(); rev_song
    } else { song }
        .join("\n")
}
