#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    SpanTooLong,
    InvalidDigit(char),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::SpanTooLong => write!(f, "Span is to long!"),
            Error::InvalidDigit(c) => write!(f, "Invalid digit '{}'", c),
        }
    }
}

impl std::error::Error for Error {}

pub fn lsp(string_digits: &str, span: usize) -> Result<u64, Error> {
    match string_digits.chars().position(|ch| !ch.is_numeric()) {
        None => match string_digits.len() >= span {
            true => Ok(string_digits
                .as_bytes()
                .windows(span)
                .map(|win| {
                    win.iter().enumerate().fold(0u64, |res, num| match num.0 {
                        0 => Into::<u64>::into((*num.1 as char).to_digit(10).unwrap()),
                        _ => res * Into::<u64>::into((*num.1 as char).to_digit(10).unwrap()),
                    })
                })
                .max()
                .unwrap()),
            false => Err(Error::SpanTooLong),
        },
        Some(x) => Err(Error::InvalidDigit(string_digits.chars().nth(x).unwrap())),
    }
}
