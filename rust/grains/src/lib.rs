pub fn square(s: u32) -> u64 {
    assert!(
        match s {
            1..=64 => true,
            _ => false
        },
        "Square must be between 1 and 64"
    );
    2u64.pow(s-1)
}

pub fn total() -> u64 {
    (1u32..=64)
        .map( |f| square(f) )
        .sum::<u64>()
}
