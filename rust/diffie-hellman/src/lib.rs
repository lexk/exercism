use rand::prelude::*;

///Right-to-left binary method used for --features big-primes calculation
///
/// TL;DR: memory efficient with `u128` to avoid 0 in `base` argument with no escape in u64,
/// when calculating with the biggest numbers of u64, for example `0xFFFF_FFFF_FFFF_FFC5`
///
/// method description:
/// A third method drastically reduces the number of operations to perform modular exponentiation,
/// while keeping the same memory footprint as in the previous method. It is a combination of the
/// Memory-efficient method and a more general principle called exponentiation by squaring
/// (also known as binary exponentiation).
/// source: https://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method
fn modular_pow_rtl_binary(modulus: u64, base_provided: u64, exponent: u64) -> u64 {
    if modulus == 1 {
        return 0;
    } else {
        let mut result: u128 = 1;
        let mut exponent= exponent as u128;
        let mut base = base_provided as u128 % modulus as u128;
        while exponent > 0 {
            if exponent % 2 == 1 {
                result = result * base % modulus as u128;
            }
            exponent >>= 1;
            base = base * base % modulus as u128
        }
        result as u64
    }
}

pub fn private_key(p: u64) -> u64 { random::<u64>() % (p - 2) + 2 }

pub fn public_key(p: u64, base: u64, private_key: u64) -> u64 {
    modular_pow_rtl_binary(p, base, private_key)
}

pub fn secret(p: u64, public_key_b: u64, private_key_a: u64) -> u64 {
    modular_pow_rtl_binary(p, public_key_b, private_key_a)
}