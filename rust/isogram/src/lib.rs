use std::collections::HashMap;

pub fn check(candidate: &str) -> bool {
    candidate.to_lowercase().chars()
        .filter(|raw_ch| raw_ch.is_alphabetic())
        .fold(HashMap::<char, i32>::new(), |mut char_hm, ch | {
            *char_hm.entry(ch).or_insert(0) += 1;char_hm})
        .values().filter(|&ch_count| ch_count > &1)
        .count() < 1
}
