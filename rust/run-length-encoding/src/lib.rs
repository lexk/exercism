use std::collections::HashMap;

pub fn encode(s: &str) -> String {
    fn transfer_result(counts: &mut HashMap<char, i32>, result: &mut String) {
        counts.iter().map(|char_count| result.push_str(&*(
            if char_count.1 == &1 { "".to_string()}
            else { char_count.1.to_string()} + char_count.0.to_string().as_str()))).for_each(drop);
        counts.clear()
    }
    s.chars().filter(|raw_ch| raw_ch.is_alphabetic() || raw_ch.is_whitespace()).fold(
        (HashMap::new(), String::new(), 0),
        |(mut counts, mut result, mut char_count), c| {
            char_count += 1;
                match counts.keys().find(|&hm_ch| hm_ch == &c) {
                    Some(_) => {
                        counts.entry(c).and_modify(|count: &mut i32| *count += 1).or_insert(1);
                        if char_count >= s.len() { transfer_result(&mut counts, &mut result); }},
                    None if !counts.is_empty() => {
                        if char_count >= s.len() { {transfer_result(&mut counts, &mut result);
                            counts.entry(c).or_insert(1)}; };
                        transfer_result(&mut counts, &mut result);
                        counts.entry(c).or_insert(1);
                    }
                    None => { counts.entry(c).or_insert(1); }
                };
            (counts, result, char_count)
        }
    ).1
}

pub fn decode(source: &str) -> String {
    source.chars().fold(
        (String::new(), String::new()),
        |(mut counts, mut result), c| {
            if c.is_numeric() { counts.push(c) } else {
                result.push_str(c.to_string()
                    .repeat(counts.parse::<usize>().unwrap_or(1)).as_str());
                counts.clear()
            };
            (counts, result)
        }
    ).1
}