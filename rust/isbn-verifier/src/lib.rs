/// Determines whether the supplied string is a valid ISBN number
pub fn is_valid_isbn(isbn: &str) -> bool {
    let isbn_len = isbn.chars().filter(|raw_ch| raw_ch.is_alphanumeric()).count();
    if isbn.contains(|h:char| h.is_alphabetic() && !(h == 'X'))
        || (![10i32, 13i32].contains(&(isbn_len as i32))) { false }
    else {
        isbn
            .chars()
            .filter(|raw_ch| raw_ch.is_alphanumeric())
            .enumerate()
            .map(|c|
                if c.1!= 'X' {c.1.to_digit(10).unwrap() * (isbn_len - c.0) as u32 }
                else { 10u32 }
            ).sum::<u32>() % 11 == 0
    }
}
