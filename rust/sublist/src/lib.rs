extern crate core;


// use std::ops::Deref;

#[derive(Debug, PartialEq)]
pub enum Comparison {
    Equal,
    Sublist,
    Superlist,
    Unequal,
}

pub fn sublist<T: PartialEq>(_first_list: &[T], _second_list: &[T]) -> Comparison {
    use Comparison::*;
    match (_first_list.len(), _second_list.len()) {
        (0,0) => Equal,
        (_,0) => Superlist,
        (0,_) => Sublist,
        (fir,sec) if fir > sec =>
            if _first_list.windows(sec).any(|sli| sli == _second_list)
            {Superlist} else {Unequal},
        (fir,sec) if fir < sec =>
            if _second_list.windows(fir).any(|sli| sli == _first_list)
            {Sublist} else {Unequal},
        (_,_) => if _first_list == _second_list
            {Equal} else {Unequal}
    }
}
