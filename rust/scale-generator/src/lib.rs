use crate::Harmony::*;
use crate::Signs::{Bemol, Diese, Plain};

// You should change this.
//
// Depending on your implementation, there are a variety of potential errors
// which might occur. They aren't checked by the test suite in order to
// allow the greatest freedom of implementation, but real libraries should
// provide useful, descriptive errors so that downstream code can react
// appropriately.
//
// One common idiom is to define an Error enum which wraps all potential
// errors. Another common idiom is to use a helper type such as failure::Error
// which does more or less the same thing but automatically.
#[derive(Debug)]
pub struct Error;

pub struct Scale {
    tonic: String,
    intervals: String,
    chromatic: Vec<String>
}

#[derive(Clone,PartialEq,Eq)]
pub enum Harmony {
    G, D, A, E, B, Fdiese, e, b, fdiese, cdiese, gdiese, ddiese, F,     // major
    Bbemol, Ebemol, Abemol, Dbemol, Gbemol, d, g, c, f, bbemol, ebemol, // minor
    a, C,                                                               // No Sharps or Flats
    Adiese, Cdiese, Ddiese, Gdiese                                      // Not-covered
}

impl Harmony {
    fn as_str(&self) -> &'static str {
        match self {
            A => "A",
            B => "B",
            C => "C",
            D => "D",
            E => "E",
            F => "F",
            G => "G",
            a => "a",
            b => "b",
            c => "c",
            d => "d",
            e => "e",
            f => "f",
            g => "g",
            Abemol => "Ab",
            Bbemol => "Bb",
            Dbemol => "Db",
            Ebemol => "Eb",
            Fdiese => "F#",
            Gbemol => "Gb",
            bbemol => "bb",
            cdiese => "c#",
            ddiese => "d#",
            ebemol => "eb",
            fdiese => "f#",
            gdiese => "g#",
            Adiese => "A#",
            Cdiese => "C#",
            Ddiese => "D#",
            Gdiese => "G#",
        }
    }
    pub fn from_string(s: String ) -> Result<Harmony, ()> {
        type Err = ();
        match &s as &str {
            "A" => Ok(A),
            "B" => Ok(B),
            "C" => Ok(C),
            "D" => Ok(D),
            "E" => Ok(E),
            "F" => Ok(F),
            "G" => Ok(G),
            "a" => Ok(a),
            "b" => Ok(b),
            "c" => Ok(c),
            "d" => Ok(d),
            "e" => Ok(e),
            "f" => Ok(f),
            "g" => Ok(g),
            "Ab" | "AB" => Ok(Abemol),
            "Bb" | "BB" => Ok(Bbemol),
            "Db" | "DB" => Ok(Dbemol),
            "Eb" | "EB" => Ok(Ebemol),
            "F#" => Ok(Fdiese),
            "Gb" | "GB" => Ok(Gbemol),
            "bb"  => Ok(bbemol),
            "c#" => Ok(cdiese),
            "d#" => Ok(ddiese),
            "eb" => Ok(ebemol),
            "f#" => Ok(fdiese),
            "g#" => Ok(gdiese),
            "A#" => Ok(Adiese),
            "C#" => Ok(Cdiese),
            "D#" => Ok(Ddiese),
            "G#" => Ok(Gdiese),
            _ => Err(()),
        }
    }
}

pub enum Signs {
    Diese,
    Bemol,
    Plain,
}

impl Signs {
    pub fn from_harmony(harm: Harmony) -> Result<Signs, ()> {
        return if BEMOL.contains(&harm) {
            Ok(Bemol)
        } else if DIESE.contains(&harm) {
            Ok(Diese)
        } else {
            Ok(Plain)
        }
    }
    pub fn into_vec(self, harmony: &Harmony) -> Vec<String> {
        type Err = ();
        let position: usize;
        let mut scale: Vec<Harmony>;
        match self {
            Diese | Plain => {
                position = SCALEDIESE.iter().position(|shift| shift == harmony).unwrap();
                scale = SCALEDIESE.to_vec(); scale.rotate_left(position);
                scale.push(harmony.clone());
                scale
            }
            Bemol => {
                position = SCALEBEMOLE.iter().position(|shift| shift == harmony).unwrap();
                scale = SCALEBEMOLE.to_vec(); scale.rotate_left(position);
                scale.push(harmony.clone());
                scale
            }
        }.iter().map(|note| note.as_str().to_string()).collect::<Vec<String>>()
    }
}

#[repr(u8)]
pub enum Interval {
    m = b'm',                                                            // MinorSecond
    M = b'M',                                                            // MajorSecond
    A = b'A',                                                            // AugmentedSecond
}

impl Interval {
    pub fn from_char(chars: char) -> Result<Interval, ()> {
        type Err = ();
        match chars {
            'm' => Ok(Interval::m),
            'M' => Ok(Interval::M),
            'A' => Ok(Interval::A),
            _ => Err(()),
        }
    }
}

const DIESE: [Harmony; 12] = [G, D, A, E, B, Fdiese, e, b, fdiese, cdiese, gdiese, ddiese];
const BEMOL: [Harmony; 12] = [F, Bbemol, Ebemol, Abemol, Dbemol, Gbemol, d, g, c, f, bbemol, ebemol];
const SCALEDIESE: [Harmony; 12] = [A, Adiese, B, C, Cdiese, D, Ddiese, E, F, Fdiese, G, Gdiese];
const SCALEBEMOLE: [Harmony; 12] = [A, Bbemol, B, C, Dbemol, D, Ebemol, E, F, Gbemol, G, Abemol];

impl Scale {
    pub fn new(tonic: &str, intervals: &str) -> Result<Scale, Error> {
        let result = Scale { tonic: tonic.to_string(), intervals: intervals.to_string(), chromatic: vec![] };
        Ok(result)
    }

    pub fn chromatic(tonic: &str) -> Result<Scale, Error> {
        Ok(
            Scale {
                tonic: tonic.parse().unwrap(),
                intervals: "".to_string(),
                chromatic: {
                    match Signs::from_harmony(Harmony::from_string(tonic.to_string()).unwrap()).unwrap() {
                        Diese | Plain => Diese,
                        Bemol => Bemol,
                    }.into_vec(&Harmony::from_string(tonic.to_string().to_uppercase()).unwrap())
                }
            }
        )
    }

    pub fn enumerate(&self) -> Vec<String> {
        if self.intervals.len() == 0 {
            Self::chromatic(&self.tonic).unwrap().chromatic
        } else {
            let current_scale = Self::chromatic(&self.tonic).unwrap();
            let mut res: Vec<String> = vec![current_scale.chromatic[0].clone()];
            let mut temp_chrom = current_scale.chromatic.as_slice()[1..].to_vec().clone();
            for interval in self.intervals.chars() {
                let interval_idx = match Interval::from_char(interval).unwrap() {
                    Interval::m => 0usize,
                    Interval::M => 1usize,
                    Interval::A => 2usize,
                };
                res.push(temp_chrom[interval_idx].clone());
                temp_chrom = temp_chrom.as_slice()[interval_idx+1..].to_vec();
            }
            res
        }
    }
}