use std::collections::{HashSet};
use crate::AngleType::{Right, Left};
use crate::DotType::{Edge, Vertex};
use crate::Direction::{BiDirectional, Horizontal, Vertical};

/// Counting calculated HashSet of Rectangles, used to initiate conversion from input lines and
/// provides actual number of found matches
pub fn count(lines: &[&str]) -> u32 {
    hs_rectangles_from_dots_data(DotsData::from_lines(lines)).iter().count() as u32
}

#[derive(Debug, Hash, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub enum DotType { Vertex, Edge }

#[derive(Debug, Hash, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub enum Direction { Vertical, Horizontal, BiDirectional }
impl Direction {
    /// Non-consuming helper for opposite Direction logic when needed, it returns NEW Direction
    pub fn opposite(&self) -> Direction {
        match self {
            Vertical => Horizontal,
            Horizontal => Vertical,
            BiDirectional => BiDirectional
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, PartialOrd)]
pub enum AngleType {Left, Right}

#[derive(Debug, Hash, Eq, PartialEq, Copy, Clone, Ord, PartialOrd)]
pub struct Dot {
    dot_type: DotType,
    x: usize,
    y: usize,
    direction: Direction
}
impl Dot {
    /// Parser of u8 representation of char according to DotType, its coordinates and maths' semantic
    pub fn from_symbol(symbol_u8: u8, x: usize, y: usize) -> Option<Dot> {
        match symbol_u8 {
            43u8 => Some(Dot{dot_type: Vertex, x, y, direction: BiDirectional }),
            124u8 => Some(Dot{dot_type: Edge, x, y, direction: Vertical }),
            45u8 => Some(Dot{dot_type: Edge, x, y, direction: Horizontal }),
            _ => None
        }
    }

    /// Getter for particular coordinate, according to Direction we building the line
    pub fn get_axis(&self, direction: Direction) -> usize {
        match direction {
            Vertical => { self.y }
            Horizontal => { self.x }
            BiDirectional => { self.x + self.y }
        }
    }
}

#[derive(Eq, PartialEq, Debug, Clone)]
pub struct DotsData {
    pub pull: Vec<Dot>
}
impl DotsData {
    /// Empty DotsData initiator
    pub fn new() -> DotsData {
        DotsData{pull: vec![]}
    }
    /// Parser of lines into DotsData needed for future abstractions
    /// It will ignore and omit any out-of-scope symbols-chars within the line
    pub fn from_lines(lines: &[&str]) -> DotsData {
        lines.iter()
            .fold(
                (DotsData::new(), 0usize),
                |( mut lines_pull, y_line), line| {
                    lines_pull.pull =
                        [
                            lines_pull.pull,
                            line.as_bytes()
                                .iter()
                                .fold((Vec::<Dot>::new(), 0usize),
                                      |(mut line_dots, x_line), symbol| {
                                          (match Dot::from_symbol(symbol.clone(), x_line, y_line) {
                                              exist @ Some(_) =>
                                                  {line_dots.push(exist.unwrap()); line_dots},
                                              None => line_dots
                                          },
                                           x_line + 1)
                                      }
                                ).0
                        ].concat();
                    (lines_pull, y_line + 1)
                }
            ).0
    }
}

#[derive(Debug, Clone, Eq, Hash, PartialEq)]
pub struct Line{
    dots_set: Vec<Dot>
}
impl Line {
    /// Initial entry point for creating the Line from Vector of Dots with known Line Direction,
    /// AngleType, Vertex as starting point
    pub fn from_vec_dots(line_candidate: Vec<Dot>, direction: Direction, angle_type: AngleType, vertex: Dot) -> Option<Line> {
        match Line::form_line(line_candidate, direction, angle_type, vertex) {
            formed_line @ Some(_) => Some( Line{dots_set: formed_line.unwrap()} ),
            None => None
        }
    }

    /// Actual driver of forming the Line and cutting leftovers if needed.
    /// Checks sets for integrity based on distance and Direction for each Dot.
    /// Returned New Vector of Dots for future line allocation
    pub fn form_line(candidate_vec: Vec<Dot>, direction: Direction, angle_type: AngleType, vertex: Dot) -> Option<Vec<Dot>>{
        /// Helper is removing the gap and following dots from Vector, to ensure integrity
        fn cut_candidate_line(angle_type: AngleType, candidate: Vec<Dot>, gap: usize) -> Vec<Dot> {
            match angle_type {
                Left => { candidate[..=gap].to_vec()}
                Right => { candidate[gap +1..].to_vec()}
            }
        }

        match candidate_vec.len() {
            0 => None,
            _ => {
                let mut candidate_line = candidate_vec;

                        match candidate_line.iter().position(|vert| vert == &vertex) {
                            None =>
                                match angle_type {
                                    Left => candidate_line.insert(0, vertex.clone()),
                                    Right => candidate_line.push(vertex.clone())
                                }
                            Some(_) => {()}
                        }
                        while
                            match angle_type {
                                Left =>
                                    candidate_line.last().unwrap() != &vertex &&
                                    candidate_line.len() > 1 &&
                                    candidate_line.last().unwrap().dot_type != Vertex,
                                Right =>
                                    candidate_line.first().unwrap() != &vertex &&
                                    candidate_line.len() > 1 &&
                                    candidate_line.last().unwrap().dot_type != Vertex
                            }  {
                            candidate_line = {candidate_line.pop(); candidate_line}
                        }
                match candidate_line
                    .windows(2)
                    .position(
                        |pointing_window|
                            (
                                (pointing_window[1].direction != direction && pointing_window[1].direction != BiDirectional)
                                || pointing_window[0].get_axis(direction) + 1 != pointing_window[1].get_axis(direction)
                            )
                            || (
                                (pointing_window[1].direction != direction && pointing_window[1].direction != BiDirectional)
                                && pointing_window[0].get_axis(direction) + 1 != pointing_window[1].get_axis(direction)
                            )
                    ) {
                    None => Some(candidate_line),
                    found_gap @ Some(_) => Line::form_line(
                        cut_candidate_line(angle_type, candidate_line, found_gap.unwrap()),
                        direction, angle_type, vertex
                    )
                }
            }
        }
    }
}

#[derive(Clone, Eq, Hash, PartialEq, Debug)]
pub struct Angle {
    vertex: Dot,
    x_shoulder: Line,
    y_shoulder: Line,
}
impl Angle {
    /// Checks provided Vertex and DotsData and tries to form an Angle according to AngleType.
    pub fn from_dots_data(vertex: &Dot, data: &DotsData, angle_type: AngleType, limit: &Dot) -> Option<Angle> {
         Angle::form_angle_from_vec_dot(data.pull
                .iter()
                .partition::<Vec<Dot>, _>(|dt_candidate|
                    (
                        match angle_type {
                            Left => dt_candidate.x > vertex.x,
                            Right => dt_candidate.x < vertex.x
                        }
                        && dt_candidate.y == vertex.y && (&dt_candidate.x >= &limit.x)) ||
                    (
                        match angle_type {
                            Left => dt_candidate.y > vertex.y,
                            Right => dt_candidate.y < vertex.y
                        }
                        && dt_candidate.x == vertex.x && (&dt_candidate.y >= &limit.y))
            ).0, vertex, angle_type)
    }

    /// Validating of previously found Vector of Dot's, tries to create a Line's,
    /// that will be used as Shoulders for the Angle and return angle if formed
    fn form_angle_from_vec_dot(angle_candidate: Vec<Dot>, vertex: &Dot, angle_type: AngleType) -> Option<Angle> {
        let (x_shoulder, y_shoulder)=
            angle_candidate
                .iter()
                .filter(|focus_dot| focus_dot != &vertex)
                .partition(|focus_dot| focus_dot.y == vertex.y && focus_dot != &vertex);
        match (Line::from_vec_dots(x_shoulder, Horizontal, angle_type, vertex.clone()),
               Line::from_vec_dots(y_shoulder, Vertical, angle_type, vertex.clone())) {
            formed_angle @ (Some(_), Some(_)) => Some(
                Angle{
                    x_shoulder: formed_angle.0.unwrap(),
                    y_shoulder: formed_angle.1.unwrap(),
                    vertex: *vertex
                }
            ),
            (_, _) => None
        }
    }

    /// Invokes forming, matching, and validation of Opposite Angle
    /// _for ex. Right-Lower for Left-Upper_
    pub fn opposite_angle(current_angle: &Angle, dots_data: &DotsData) -> Option<Angle> {
        match dots_data.pull.iter().position(|focus_dot| focus_dot == &Dot{
            dot_type: Vertex,
            x: current_angle.x_shoulder.dots_set.iter().last().unwrap().x,
            y: current_angle.y_shoulder.dots_set.iter().last().unwrap().y,
            direction: BiDirectional,
        }) {
            None => None,
            opposite_vertex_idx @ Some(_) => {
                let opposite_vertex = dots_data.pull[opposite_vertex_idx.unwrap()];
                match dots_data.pull.iter().position(|focus_dot| focus_dot == &opposite_vertex) {
                    Some(_) => 
                        match Angle::from_dots_data(
                            &opposite_vertex,
                            &dots_data,
                            Right,
                            &current_angle.vertex
                        ) { 
                            None => None, 
                            found_candidate @ Some(_) => { 
                                Angle::validate_opposite_angle( &current_angle, found_candidate.unwrap()) 
                            } 
                        },
                    None => None
                } 
            }}
        }

    /// Non-consuming Helper that **creates NEW Vector** of Dots from Angle's shoulder,
    /// that is picked according to provided shoulders Direction.
    fn pick_shoulder_direction(&self, direction: Direction) -> Vec<Dot>{
        match direction {
            Vertical => self.y_shoulder.dots_set.clone(),
            Horizontal => self.x_shoulder.dots_set.clone(),
            BiDirectional => Vec::<Dot>::new()
        }
    }

    /// Validation of Opposite Angle build on length & symmetry of swapped coordinates (x->y; y->x)

    fn validate_opposite_angle(current_angle: &Angle, candidate_opposite: Angle) -> Option<Angle> {
        /// Helper searching for abnormalities, return value has semantically reversed logic,
        /// so the found value will invalidate the shoulder
        fn validate_shoulder(current_angle: &Angle, candidate_opposite: &Angle, course: Direction) -> Option<()> {
            match 
                current_angle
                    .pick_shoulder_direction(course)
                    .len() == 
                candidate_opposite.
                    pick_shoulder_direction(course)
                    .len() {
                true => {
                    match current_angle
                        .pick_shoulder_direction(course)
                        .iter()
                        .zip(
                            candidate_opposite
                                .pick_shoulder_direction(course)
                        )
                        .find(|pair|
                            pair.0.get_axis(course) != pair.1.get_axis(course) + 1
                                && pair.0.get_axis(course.opposite())
                                == candidate_opposite
                                .pick_shoulder_direction(course.opposite())
                                .last()
                                .unwrap()
                                .get_axis(course.opposite())
                        ) {
                        Some(_) => Some(()),
                        None => None
                    }
                },
                false => {Some(())}
            }
        }
        match (
            ( validate_shoulder(&current_angle, &candidate_opposite, Horizontal),
              validate_shoulder(&current_angle, &candidate_opposite, Vertical) ),
            (
                candidate_opposite.vertex.x == current_angle.x_shoulder.dots_set.last().unwrap().x,
                candidate_opposite.vertex.y == current_angle.y_shoulder.dots_set.last().unwrap().y
            )
        ) {
            ((None, None), (true, true)) => Some(candidate_opposite),
            _ => None
        }
    }

}



#[derive(Debug, Clone, Eq, Hash, PartialEq)]
pub struct Rectangle {
    left_angle: Angle,
    right_angle: Angle
}
impl Rectangle {
    /// Searching function for matching composition of Left-Upper Angle & Right-Lower Angle.
    ///
    /// It tries to build a Rectangle, having Vertex of Left-Upper corner Angle
    /// it invokes building Angle with Vertex Dot, validates if it has an Opposite (Right-Lower)
    /// corner Angle matching it and if found provides Vector of Rectangles.
    ///
    /// In Case of mismatch, it'll check for other possible Left-Upper Angles for this Vertex and
    /// will try to match those accordingly. In case of match absence, it'll iterate over other
    /// Vertex dots in DotsData and keep retrying cycle for all of them
    pub fn vec_rectangle_from_dots_data(vertex: &Dot, data: &DotsData) -> Option<Vec<Rectangle>> {
        /// Non-consuming Helper to cut the Shoulder (x asics or y asics laying dots) for found Angle.
        /// Main usage to verify if there's more than one possible Angle to build from the current
        /// already found set of dots.
        fn shoulder_cut(angle: &Angle, direction: Direction, vertex: &Dot, data: &DotsData) -> Option<Vec<Rectangle>> {
            match angle
                .pick_shoulder_direction(direction)
                .iter()
                .filter(
                    |f_dot|
                        f_dot.dot_type == Vertex
                )
                .collect::<Vec<&Dot>>()
            {
                found_vert @ _
                if found_vert.len() > 1 =>
                    Rectangle::vec_rectangle_from_dots_data(
                        vertex,
                        &DotsData{
                            pull:
                                data.pull

                                    .iter()
                                    .cloned().
                                    filter(
                                        |d_dot|
                                            &d_dot != found_vert.last().unwrap()
                                    )
                                    .collect::<Vec<Dot>>()
                        }
                    ),
                _ => None
            }
        }
        match Angle::from_dots_data(vertex, data, Left, vertex) {
            found_angle @ Some(_) => {
                let current_found_angle = found_angle.unwrap();
                match Angle::opposite_angle(&current_found_angle, data) {
                    found_opposite @ Some(_) => Some(
                        vec![
                            Rectangle {
                                left_angle: current_found_angle,
                                right_angle: found_opposite.unwrap()
                            }
                        ]
                    ),
                    None =>
                        match (
                            match shoulder_cut(&current_found_angle, Vertical, vertex, data) {
                                found_x_cut_shoulder @ Some(_) => found_x_cut_shoulder,
                                None => shoulder_cut(&current_found_angle, Horizontal, vertex, data)
                            },
                            match shoulder_cut(&current_found_angle, Horizontal, vertex, data) {
                                found_y_cut_shoulder @ Some(_) => found_y_cut_shoulder,
                                None => shoulder_cut(&current_found_angle, Vertical, vertex, data)

                            }
                        ) {
                            found_both @
                            (Some(_), Some(_)) =>
                                Some([found_both.0.unwrap(), found_both.1.unwrap()].concat()),
                            (found_vert @ Some(_), None) => found_vert,
                            (None, found_hor @ Some(_)) => found_hor,
                            _ => None
                        }
                }
            },
            None => None
        }
    }

    /// Conversion without consuming from Rectangle to DotsData with returned NEW DotsData region,
    /// border of which is converted Rectangle. This is helper f-n to ease checking of built-in
    /// rectangles inside of the same region of DotsData.
    pub fn inner_dots_data(&self, dots_data: &DotsData) -> DotsData {
        DotsData{
            pull:
                dots_data
                    .pull
                    .iter()
                    .cloned()
                    .filter(
                        |focus|
                            focus.x >= self.left_angle.vertex.x &&
                            focus.y >= self.left_angle.vertex.y &&
                            focus.x <= self.right_angle.vertex.x &&
                            focus.y <= self.right_angle.vertex.y &&
                            focus != &self.right_angle.vertex
                    )
                    .collect()
        }
    }
}

/// Main function driving conversions from DotsData to Rectangle,
/// Checking each of found for deeper layer of built-in rectangles
/// and accumulating Rectangles in HashSet
fn hs_rectangles_from_dots_data(dots_data: DotsData) -> HashSet<Rectangle>{
    let mut result_hs_rectangles =
        dots_data.pull.iter()
            .fold(
                HashSet::<Rectangle>::new(),
                |mut rect_iter, selected_dot| {
                    match selected_dot.dot_type {
                        Vertex =>
                            match Rectangle::vec_rectangle_from_dots_data(selected_dot, &dots_data) {
                                found_rectangle @
                                Some(_) =>
                                    {
                                        found_rectangle.unwrap()
                                            .iter()
                                            .cloned()
                                            .map(
                                                |rectangle_focus|
                                                    rect_iter.insert(rectangle_focus)).count();
                                        rect_iter
                                    },
                                None => rect_iter
                            }
                        _ => rect_iter
                    }
                }
            );

    let vec_of_hs_internal_rectangles = result_hs_rectangles
        .iter()
        .cloned()
        .map(
            |focus_r|
                if focus_r.right_angle.vertex.x - focus_r.left_angle.vertex.x != 1
                    && focus_r.right_angle.vertex.y - focus_r.left_angle.vertex.y != 1 {
                    hs_rectangles_from_dots_data(focus_r.inner_dots_data(&dots_data))
                } else {
                    let mut same_sh = HashSet::<Rectangle>::new();
                    same_sh.insert(focus_r);
                    same_sh
                }
        )
        .collect::<Vec<HashSet<Rectangle>>>();

    for hs in vec_of_hs_internal_rectangles {
        result_hs_rectangles.extend(hs)
    }
    result_hs_rectangles
}

/// Partial test coverage section on a component level helpers
#[cfg(test)]
mod component_rectangle {
    use super::*;
    #[test]
    fn test_dot_parse_dot_from_char() {
        assert_eq!(
            Some(Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional }),
            Dot::from_symbol('+' as u8, 0, 0));
        assert_eq!(Some(Dot { dot_type: Edge, x: 111, y: 111, direction: Vertical  }),
                   Dot::from_symbol('|' as u8, 111, 111));
        assert_eq!(Some(Dot { dot_type: Edge, x: 00, y: 254, direction: Horizontal }),
                   Dot::from_symbol('-' as u8, 00, 254));
        assert_eq!(None, Dot::from_symbol(' ' as u8, 9, 2));
        assert_eq!(None, Dot::from_symbol('5' as u8, 123, 321));
    }

    #[test]
    fn test_dots_data_from_lines_parsing_with_white_space() {
        let dots_data = DotsData::from_lines(&["+-+-+ +"]);
        assert_eq!(
            DotsData {
                pull:
                    vec![
                        Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional },
                        Dot { dot_type: Edge, x: 1, y: 0, direction: Horizontal },
                        Dot { dot_type: Vertex, x: 2, y: 0, direction: BiDirectional },
                        Dot { dot_type: Edge, x: 3, y: 0, direction: Horizontal },
                        Dot { dot_type: Vertex, x: 4, y: 0, direction: BiDirectional },
                        Dot { dot_type: Vertex, x: 6, y: 0, direction: BiDirectional }
                    ]
            },
            dots_data)
    }

    #[test]
    fn test_line_form_line_consistency_both_directions_with_gap() {

        // given
        let unexpected_form_result_vertical = vec![
            Dot { dot_type: Vertex, x: 0, y: 1, direction: BiDirectional },
            Dot { dot_type: Edge, x: 0, y: 2, direction: Vertical },
            Dot { dot_type: Edge, x: 0, y: 3, direction: Vertical },
            Dot { dot_type: Edge, x: 0, y: 4, direction: Vertical },
            Dot { dot_type: Vertex, x: 0, y: 5, direction: BiDirectional }
        ];

        // when
        let calculated_bad_line_result_vertical = Line::form_line(
            vec![
                Dot::from_symbol('+' as u8, 0, 1).unwrap(),
                Dot::from_symbol('|' as u8, 0, 2).unwrap(),
                Dot::from_symbol('-' as u8, 0, 3).unwrap(),
                Dot::from_symbol('|' as u8, 0, 4).unwrap(),
                Dot::from_symbol('+' as u8, 0, 5).unwrap(),
                Dot::from_symbol('+' as u8, 0, 7).unwrap()
            ], Vertical, Left, Dot::from_symbol('+' as u8, 0, 1).unwrap());

        // then
        assert_ne!(calculated_bad_line_result_vertical, Some(unexpected_form_result_vertical));

        // given
        let expected_form_result_horizontal = vec![
            Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional },
            Dot { dot_type: Edge, x: 1, y: 0, direction: Horizontal },
            Dot { dot_type: Vertex, x: 2, y: 0, direction: BiDirectional },
            Dot { dot_type: Edge, x: 3, y: 0, direction: Horizontal },
            Dot { dot_type: Vertex, x: 4, y: 0, direction: BiDirectional }
        ];

        // when
        let calculated_result_horizontal = Line::form_line(
            vec![
                Dot::from_symbol('+' as u8, 0, 0).unwrap(),
                Dot::from_symbol('-' as u8, 1, 0).unwrap(),
                Dot::from_symbol('+' as u8, 2, 0).unwrap(),
                Dot::from_symbol('-' as u8, 3, 0).unwrap(),
                Dot::from_symbol('+' as u8, 4, 0).unwrap(),
                Dot::from_symbol('+' as u8, 6, 0).unwrap()
            ], Horizontal, Left, Dot::from_symbol('+' as u8, 0, 0).unwrap());

        // then
        assert_eq!(calculated_result_horizontal, Some(expected_form_result_horizontal));

        // given
        let expected_form_result_vertical =
            vec![
                Dot { dot_type: Vertex, x: 0, y: 1, direction: BiDirectional },
                Dot { dot_type: Edge, x: 0, y: 2, direction: Vertical },
                Dot { dot_type: Vertex, x: 0, y: 3, direction: BiDirectional },
                Dot { dot_type: Edge, x: 0, y: 4, direction: Vertical },
                Dot { dot_type: Vertex, x: 0, y: 5, direction: BiDirectional }
            ];

        // when
        let calculated_result_vertical = Line::form_line(
            vec![
                Dot::from_symbol('+' as u8, 0, 1).unwrap(),
                Dot::from_symbol('|' as u8, 0, 2).unwrap(),
                Dot::from_symbol('+' as u8, 0, 3).unwrap(),
                Dot::from_symbol('|' as u8, 0, 4).unwrap(),
                Dot::from_symbol('+' as u8, 0, 5).unwrap(),
                Dot::from_symbol('+' as u8, 0, 7).unwrap()
            ], Vertical, Left, Dot::from_symbol('+' as u8, 0, 1).unwrap());

        //then
        assert_eq!(calculated_result_vertical, Some(expected_form_result_vertical));
    }

    #[test]
    fn test_line_from_vec_dots_parsing_with_gap() {
        // given
        let unexpected_from_result_vertical =
            Line {
                dots_set: vec![
                    Dot { dot_type: Vertex, x: 9, y: 1, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 9, y: 2, direction: Vertical },
                    Dot { dot_type: Edge, x: 9, y: 3, direction: Vertical },
                    Dot { dot_type: Edge, x: 9, y: 4, direction: Vertical },
                    Dot { dot_type: Vertex, x: 9, y: 5, direction: BiDirectional }
                ]
            };

        // when
        let calculated_bad_line_result_vertical = Line::from_vec_dots(
            vec![
                Dot::from_symbol('+' as u8, 9, 1).unwrap(),
                Dot::from_symbol('|' as u8, 9, 2).unwrap(),
                Dot::from_symbol('-' as u8, 9, 3).unwrap(),
                Dot::from_symbol('|' as u8, 9, 4).unwrap(),
                Dot::from_symbol('+' as u8, 9, 5).unwrap(),
                Dot::from_symbol('+' as u8, 9, 7).unwrap()
            ], Vertical, Left, Dot::from_symbol('+' as u8, 9, 1).unwrap());

        // then
        assert_ne!(calculated_bad_line_result_vertical, Some(unexpected_from_result_vertical));

        // given
        let expected_from_result_horizontal =
            Line {
                dots_set:
                vec![
                    Dot { dot_type: Vertex, x: 0, y: 8, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 1, y: 8, direction: Horizontal },
                    Dot { dot_type: Vertex, x: 2, y: 8, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 3, y: 8, direction: Horizontal },
                    Dot { dot_type: Vertex, x: 4, y: 8, direction: BiDirectional }
                ]
            };

        // when
        let calculated_result_horizontal = Line::from_vec_dots(
            vec![
                Dot::from_symbol('+' as u8, 0, 8).unwrap(),
                Dot::from_symbol('-' as u8, 1, 8).unwrap(),
                Dot::from_symbol('+' as u8, 2, 8).unwrap(),
                Dot::from_symbol('-' as u8, 3, 8).unwrap(),
                Dot::from_symbol('+' as u8, 4, 8).unwrap(),
                Dot::from_symbol('+' as u8, 6, 8).unwrap()
            ], Horizontal, Left, Dot::from_symbol('+' as u8, 0, 8).unwrap());

        // then
        assert_eq!(Some(expected_from_result_horizontal), calculated_result_horizontal);

        // given
        let expected_from_result_vertical =
            Line {
                dots_set: vec![
                    Dot { dot_type: Vertex, x: 0, y: 1, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 0, y: 2, direction: Vertical },
                    Dot { dot_type: Vertex, x: 0, y: 3, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 0, y: 4, direction: Vertical },
                    Dot { dot_type: Vertex, x: 0, y: 5, direction: BiDirectional }
                ]
            };

        // when
        let calculated_result_vertical =
            Line::from_vec_dots(
                vec![
                    Dot::from_symbol('+' as u8, 0, 1).unwrap(),
                    Dot::from_symbol('|' as u8, 0, 2).unwrap(),
                    Dot::from_symbol('+' as u8, 0, 3).unwrap(),
                    Dot::from_symbol('|' as u8, 0, 4).unwrap(),
                    Dot::from_symbol('+' as u8, 0, 5).unwrap(),
                Dot::from_symbol('+' as u8, 0, 7).unwrap()
                ],
                Vertical,
                Left,
                Dot::from_symbol('+' as u8, 0, 1)
                    .unwrap());

        // then
        assert_eq!(calculated_result_vertical, Some(expected_from_result_vertical));
    }

    #[test]
    fn test_angle_from_dots_data() {
        // given
        let vertex = Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional };
        let dots_data =
            DotsData {
                pull: vec![
                    Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 1, y: 0, direction: Horizontal },
                    Dot { dot_type: Vertex, x: 2, y: 0, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 3, y: 0, direction: Horizontal },
                    Dot { dot_type: Vertex, x: 4, y: 0, direction: BiDirectional },
                    Dot { dot_type: Vertex, x: 6, y: 0, direction: BiDirectional },
                    Dot { dot_type: Vertex, x: 0, y: 1, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 0, y: 2, direction: Vertical },
                    Dot { dot_type: Vertex, x: 0, y: 3, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 0, y: 4, direction: Vertical },
                    Dot { dot_type: Vertex, x: 0, y: 5, direction: BiDirectional },
                    Dot { dot_type: Vertex, x: 0, y: 7, direction: BiDirectional }
                ]
            };

        // when
        let expected_angle =
            Angle{
                vertex: Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional },
                x_shoulder:
                    Line {
                        dots_set: vec![
                            Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional },
                            Dot { dot_type: Edge, x: 1, y: 0, direction: Horizontal },
                            Dot { dot_type: Vertex, x: 2, y: 0, direction: BiDirectional },
                            Dot { dot_type: Edge, x: 3, y: 0, direction: Horizontal },
                            Dot { dot_type: Vertex, x: 4, y: 0, direction: BiDirectional }
                        ]
                },
                y_shoulder:
                    Line {
                        dots_set:
                            vec![
                                Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional },
                                Dot { dot_type: Vertex, x: 0, y: 1, direction: BiDirectional },
                                Dot { dot_type: Edge, x: 0, y: 2, direction: Vertical },
                                Dot { dot_type: Vertex, x: 0, y: 3, direction: BiDirectional },
                                Dot { dot_type: Edge, x: 0, y: 4, direction: Vertical },
                                Dot { dot_type: Vertex, x: 0, y: 5, direction: BiDirectional }
                            ]
                    }
            };

        // then
        assert_eq!(
            Some(expected_angle),
            Angle::from_dots_data(&vertex, &dots_data, Left, &vertex)
        )
    }

    #[test]
    fn test_angle_opposite_angle(){

        // given
        const VISUAL: &[&str; 8] = &[
            "+-+-+ +",
            "+   |  ",
            "|   +  ",
            "+   |  ",
            "|   +  ",
            "+-+-+ +",
            "       ",
            "+      "
        ];
        let current_angle =
            Angle{
                vertex: Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional },
                x_shoulder:
                Line {
                    dots_set: vec![
                        Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional },
                        Dot { dot_type: Edge, x: 1, y: 0, direction: Horizontal },
                        Dot { dot_type: Vertex, x: 2, y: 0, direction: BiDirectional },
                        Dot { dot_type: Edge, x: 3, y: 0, direction: Horizontal },
                        Dot { dot_type: Vertex, x: 4, y: 0, direction: BiDirectional }
                    ]
                },
                y_shoulder:
                Line {
                    dots_set:
                    vec![
                        Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional },
                        Dot { dot_type: Vertex, x: 0, y: 1, direction: BiDirectional },
                        Dot { dot_type: Edge, x: 0, y: 2, direction: Vertical },
                        Dot { dot_type: Vertex, x: 0, y: 3, direction: BiDirectional },
                        Dot { dot_type: Edge, x: 0, y: 4, direction: Vertical },
                        Dot { dot_type: Vertex, x: 0, y: 5, direction: BiDirectional }
                    ]
                }
            };
        let dots_data =
            DotsData {
                pull: vec![
                    Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 1, y: 0, direction: Horizontal },
                    Dot { dot_type: Vertex, x: 2, y: 0, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 3, y: 0, direction: Horizontal },
                    Dot { dot_type: Vertex, x: 4, y: 0, direction: BiDirectional },
                    Dot { dot_type: Vertex, x: 6, y: 0, direction: BiDirectional },
                    Dot { dot_type: Vertex, x: 0, y: 1, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 0, y: 2, direction: Vertical },
                    Dot { dot_type: Vertex, x: 0, y: 3, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 0, y: 4, direction: Vertical },
                    Dot { dot_type: Vertex, x: 0, y: 5, direction: BiDirectional },
                    Dot { dot_type: Vertex, x: 0, y: 7, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 1, y: 5, direction: Horizontal },
                    Dot { dot_type: Vertex, x: 2, y: 5, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 3, y: 5, direction: Horizontal },
                    Dot { dot_type: Vertex, x: 4, y: 5, direction: BiDirectional },
                    Dot { dot_type: Vertex, x: 6, y: 5, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 4, y: 1, direction: Vertical },
                    Dot { dot_type: Vertex, x: 4, y: 2, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 4, y: 3, direction: Vertical },
                    Dot { dot_type: Vertex, x: 4, y: 4, direction: BiDirectional },
                    Dot { dot_type: Vertex, x: 4, y: 5, direction: BiDirectional }
                ]
            };

        // when
        let expected_opposite_angle =
            Angle {
                vertex:
                    Dot { dot_type: Vertex, x: 4, y: 5, direction: BiDirectional },
                x_shoulder:
                    Line {
                        dots_set: vec![
                            Dot { dot_type: Vertex, x: 0, y: 5, direction: BiDirectional },
                            Dot { dot_type: Edge, x: 1, y: 5, direction: Horizontal },
                            Dot { dot_type: Vertex, x: 2, y: 5, direction: BiDirectional },
                            Dot { dot_type: Edge, x: 3, y: 5, direction: Horizontal },
                            Dot { dot_type: Vertex, x: 4, y: 5, direction: BiDirectional }
                        ]
                    },
                y_shoulder:
                    Line {
                        dots_set: vec![
                            Dot { dot_type: Vertex, x: 4, y: 0, direction: BiDirectional },
                            Dot { dot_type: Edge, x: 4, y: 1, direction: Vertical },
                            Dot { dot_type: Vertex, x: 4, y: 2, direction: BiDirectional },
                            Dot { dot_type: Edge, x: 4, y: 3, direction: Vertical },
                            Dot { dot_type: Vertex, x: 4, y: 4, direction: BiDirectional },
                            Dot { dot_type: Vertex, x: 4, y: 5, direction: BiDirectional }
                        ]
                    }
            };

        // then
        assert_eq!(Some(expected_opposite_angle), Angle::opposite_angle(&current_angle, &dots_data))
    }

    #[test]
    fn test_rectangle_from_dots_data() {

        // given
        let vertex = Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional };
        let data =
            DotsData {
                pull: vec![
                    Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 1, y: 0, direction: Horizontal },
                    Dot { dot_type: Vertex, x: 2, y: 0, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 3, y: 0, direction: Horizontal },
                    Dot { dot_type: Vertex, x: 4, y: 0, direction: BiDirectional },
                    Dot { dot_type: Vertex, x: 6, y: 0, direction: BiDirectional },
                    Dot { dot_type: Vertex, x: 0, y: 1, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 0, y: 2, direction: Vertical },
                    Dot { dot_type: Vertex, x: 0, y: 3, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 0, y: 4, direction: Vertical },
                    Dot { dot_type: Vertex, x: 0, y: 5, direction: BiDirectional },
                    Dot { dot_type: Vertex, x: 0, y: 7, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 1, y: 5, direction: Horizontal },
                    Dot { dot_type: Vertex, x: 2, y: 5, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 3, y: 5, direction: Horizontal },
                    Dot { dot_type: Vertex, x: 4, y: 5, direction: BiDirectional },
                    Dot { dot_type: Vertex, x: 6, y: 5, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 4, y: 1, direction: Vertical },
                    Dot { dot_type: Vertex, x: 4, y: 2, direction: BiDirectional },
                    Dot { dot_type: Edge, x: 4, y: 3, direction: Vertical },
                    Dot { dot_type: Vertex, x: 4, y: 4, direction: BiDirectional },
                    Dot { dot_type: Vertex, x: 4, y: 5, direction: BiDirectional }
                ]
            };

        // when
        let expected_rectangle =
            Rectangle{
                left_angle: Angle {
                    vertex: Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional },
                    x_shoulder:
                        Line {
                            dots_set: vec![
                                Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional },
                                Dot { dot_type: Edge, x: 1, y: 0, direction: Horizontal },
                                Dot { dot_type: Vertex, x: 2, y: 0, direction: BiDirectional },
                                Dot { dot_type: Edge, x: 3, y: 0, direction: Horizontal },
                                Dot { dot_type: Vertex, x: 4, y: 0, direction: BiDirectional }
                            ]
                        },
                    y_shoulder:
                        Line {
                            dots_set:
                            vec![
                                Dot { dot_type: Vertex, x: 0, y: 0, direction: BiDirectional },
                                Dot { dot_type: Vertex, x: 0, y: 1, direction: BiDirectional },
                                Dot { dot_type: Edge, x: 0, y: 2, direction: Vertical },
                                Dot { dot_type: Vertex, x: 0, y: 3, direction: BiDirectional },
                                Dot { dot_type: Edge, x: 0, y: 4, direction: Vertical },
                                Dot { dot_type: Vertex, x: 0, y: 5, direction: BiDirectional }
                            ]
                        }
                },
                right_angle:
                    Angle {
                        vertex:
                            Dot { dot_type: Vertex, x: 4, y: 5, direction: BiDirectional },
                        x_shoulder:
                            Line {
                                dots_set: vec![
                                    Dot { dot_type: Vertex, x: 0, y: 5, direction: BiDirectional },
                                    Dot { dot_type: Edge, x: 1, y: 5, direction: Horizontal },
                                    Dot { dot_type: Vertex, x: 2, y: 5, direction: BiDirectional },
                                    Dot { dot_type: Edge, x: 3, y: 5, direction: Horizontal },
                                    Dot { dot_type: Vertex, x: 4, y: 5, direction: BiDirectional },
                                ]
                            },
                        y_shoulder:
                            Line {
                                dots_set: vec![
                                    Dot { dot_type: Vertex, x: 4, y: 0, direction: BiDirectional },
                                    Dot { dot_type: Edge, x: 4, y: 1, direction: Vertical },
                                    Dot { dot_type: Vertex, x: 4, y: 2, direction: BiDirectional },
                                    Dot { dot_type: Edge, x: 4, y: 3, direction: Vertical },
                                    Dot { dot_type: Vertex, x: 4, y: 4, direction: BiDirectional },
                                    Dot { dot_type: Vertex, x: 4, y: 5, direction: BiDirectional }
                                ]
                            }
                    }
            };
        let found_rectangle = Rectangle::vec_rectangle_from_dots_data(&vertex, &data);

        // then
        assert_eq!(vec![expected_rectangle], found_rectangle.unwrap())
    }
}