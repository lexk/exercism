use std::fmt::{Display, Formatter, Result};

pub struct Roman {
    roman_num: String
}

impl Display for Roman {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "{}", self.roman_num)
    }
}

impl From<u32> for Roman {
    fn from(num: u32) -> Self {
        // TODO: mod 5 and enum
        Self { roman_num:
        num
            .to_string()
            .chars()
            .enumerate()
            .map(|rad_pow|
                match (
                    num.to_string().len() - rad_pow.0,
                    rad_pow.1.to_digit(10).unwrap() as usize) {
                    thousand @ (4, _) => "M".repeat(thousand.1),
                    (3, 9) => "CM".to_owned(),
                    (3, 4) => "CD".to_owned(),
                    f_hundred @ (3, _) if f_hundred.1 >=5 => "D".to_owned() + &"C".repeat(f_hundred.1 - 5),
                    hundred @ (3, _) => "C".repeat(hundred.1),
                    (2, 9) => "XC".to_owned(),
                    (2, 4) => "XL".to_owned(),
                    fifty @ (2, _) if fifty.1 >= 5 => "L".to_owned() + &"X".repeat(fifty.1 - 5),
                    tens @ (2, _) => "X".repeat(tens.1),
                    (1, 9) => "IX".to_owned(),
                    (1, 4) => "IV".to_owned(),
                    five @ (1, _) if five.1 >= 5 => "V".to_owned() + &"I".repeat(five.1 - 5),
                    units @ (1, _) => "I".repeat(units.1),
                    _ => "wow".to_owned()
                }
            ).collect()
        }
    }
}
